package com.skripsi.randynt.gms.classDef;

public class Message {
    private String id_message;
    private String id_chatroom;
    private String id_sender;
    private String sender_name;
    private String message;
    private String status_message;
    private String time;
    private String listener;

    public Message(String id_message, String id_chatroom, String id_sender, String sender_name, String message, String status_message, String time, String listener) {
        this.id_message = id_message;
        this.id_chatroom = id_chatroom;
        this.id_sender = id_sender;
        this.sender_name = sender_name;
        this.message = message;
        this.status_message = status_message;
        this.time = time;
        this.listener = listener;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id_message='" + id_message + '\'' +
                ", id_chatroom='" + id_chatroom + '\'' +
                ", id_sender='" + id_sender + '\'' +
                ", sender_name='" + sender_name + '\'' +
                ", message='" + message + '\'' +
                ", status_message='" + status_message + '\'' +
                ", time='" + time + '\'' +
                ", listener='" + listener + '\'' +
                '}';
    }

    public String getId_message() {
        return id_message;
    }

    public void setId_message(String id_message) {
        this.id_message = id_message;
    }

    public String getId_chatroom() {
        return id_chatroom;
    }

    public void setId_chatroom(String id_chatroom) {
        this.id_chatroom = id_chatroom;
    }

    public String getId_sender() {
        return id_sender;
    }

    public void setId_sender(String id_sender) {
        this.id_sender = id_sender;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus_message() {
        return status_message;
    }

    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getListener() {
        return listener;
    }

    public void setListener(String listener) {
        this.listener = listener;
    }
}
