package com.skripsi.randynt.gms.Admin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.AdminEvent.AddEventActivity;
import com.skripsi.randynt.gms.AdminEvent.AttendEventActivity;
import com.skripsi.randynt.gms.AdminEvent.EventListActivity;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.AdminEventMenuListAdapter;
import com.skripsi.randynt.gms.classDef.AdminEventMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AdminEventActivity extends AppCompatActivity {
    private ArrayList<AdminEventMenu> menuList = new ArrayList<>();
    private ListView mListView;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/";
    private List<String> placeList;
    private List<Integer> placeListID;
    private List<String> eventypeList;
    private List<Integer> eventypeListID;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_event);
        mListView = (ListView)findViewById(android.R.id.list);

        setMenu();
        placeList = new ArrayList<String>();
        placeListID = new ArrayList<Integer>();
        eventypeList = new ArrayList<String>();
        eventypeListID = new ArrayList<Integer>();
        loadPlace();
        loadEventype();
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("Administrative Area");


        AdminEventMenuListAdapter adapter = new AdminEventMenuListAdapter(this.getBaseContext(),R.layout.adapter_profile_layout,menuList);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Class cls =menuList.get(position).getAct();
                Intent intent = new Intent(AdminEventActivity.this, cls);
                if(position == 0) {
                    intent.putStringArrayListExtra("placedata", (ArrayList<String>) placeList);
                    intent.putIntegerArrayListExtra("placedataID", (ArrayList<Integer>) placeListID);
                    intent.putStringArrayListExtra("eventypedata", (ArrayList<String>) eventypeList);
                    intent.putIntegerArrayListExtra("eventypedataID", (ArrayList<Integer>) eventypeListID);
                }
                startActivity(intent);

            }
        });
    }public void setMenu()
    {
        menuList.add(new AdminEventMenu("ic_add_cg_meeting_red_40dp","Add New Event",AddEventActivity.class));
        menuList.add(new AdminEventMenu("ic_cgattend_red_40dp","Event Attendance",AttendEventActivity.class));
        menuList.add(new AdminEventMenu("ic_event_list_red_40dp","Event List",EventListActivity.class));
    }

    private void loadPlace()
    {
        String URL_Place = this.URL_PRODUCTS+"place";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_Place,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray userdata = jsonObject.getJSONArray("data");
                            for (int i = 0; i < length; i++) {
                                JSONObject user = userdata.getJSONObject(i);
                                placeList.add(
                                        user.getString("nama_m_place"));
                                placeListID.add(user.getInt("id_m_place"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void loadEventype()
    {
        String URL_Place = this.URL_PRODUCTS+"eventype";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_Place,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray userdata = jsonObject.getJSONArray("data");
                            for (int i = 0; i < length; i++) {
                                JSONObject user = userdata.getJSONObject(i);
                                eventypeList.add(
                                        user.getString("nama_m_event_type"));
                                eventypeListID.add(user.getInt("id_m_event_type"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }


    }

