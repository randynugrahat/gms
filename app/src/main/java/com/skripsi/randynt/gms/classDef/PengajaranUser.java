package com.skripsi.randynt.gms.classDef;

public class PengajaranUser {
    private String id_pengajaran_attend;
    private String id_m_audience_fk;
    private String id_pengajaran_type_fk;

    public PengajaranUser(String id_pengajaran_attend, String id_m_audience_fk, String id_pengajaran_type_fk) {
        this.id_pengajaran_attend = id_pengajaran_attend;
        this.id_m_audience_fk = id_m_audience_fk;
        this.id_pengajaran_type_fk = id_pengajaran_type_fk;
    }

    public String getId_pengajaran_attend() {
        return id_pengajaran_attend;
    }

    public void setId_pengajaran_attend(String id_pengajaran_attend) {
        this.id_pengajaran_attend = id_pengajaran_attend;
    }

    public String getId_m_audience_fk() {
        return id_m_audience_fk;
    }

    public void setId_m_audience_fk(String id_m_audience_fk) {
        this.id_m_audience_fk = id_m_audience_fk;
    }

    public String getId_pengajaran_type_fk() {
        return id_pengajaran_type_fk;
    }

    public void setId_pengajaran_type_fk(String id_pengajaran_type_fk) {
        this.id_pengajaran_type_fk = id_pengajaran_type_fk;
    }

    @Override
    public String toString() {
        return "PengajaranUser{" +
                "id_pengajaran_attend='" + id_pengajaran_attend + '\'' +
                ", id_m_audience_fk='" + id_m_audience_fk + '\'' +
                ", id_pengajaran_type_fk='" + id_pengajaran_type_fk + '\'' +
                '}';
    }
}
