package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Comment;

import java.util.List;

public class CommentListAdapter extends ArrayAdapter<Comment> {
    private static final String TAG = "CommentListAdapter";
    private Context mContext;
    int mResource;

    public CommentListAdapter(@NonNull Context context, int resource, @NonNull List<Comment> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        int idComment = getItem(position).getId_comment();
        int idAudience = getItem(position).getId_audience();
        int idNews = getItem(position).getId_news();
        String audience_name = getItem(position).getAudience_name();
        String comment = getItem(position).getName();
        String date = getItem(position).getDate();
        String datesplit[] = date.split(" ");
        Comment commentclass= new Comment(idComment,idNews,idAudience,audience_name,comment,date);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);
Log.e("LIST",comment);
        // TextView TvPicture =(TextView) convertView.findViewById(R.id.name_textview);
        TextView UserName =(TextView) convertView.findViewById(R.id.commentname_textview);
        TextView Commentdata =(TextView) convertView.findViewById(R.id.commentdata_textview);

        TextView date_textview =(TextView) convertView.findViewById(R.id.commentdate_textview);

        UserName.setText(audience_name);
        Commentdata.setText(comment);
        date_textview.setText(datesplit[0]);
        return convertView;
    }

}