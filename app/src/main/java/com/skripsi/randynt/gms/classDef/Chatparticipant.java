package com.skripsi.randynt.gms.classDef;

public class Chatparticipant {
    private String id_chat_participant;
    private String id_m_audience_fk;
    private String id_chatroom_fk;
    private String created_room;
    private String nama_m_audience;

    public Chatparticipant(String id_chat_participant, String id_m_audience_fk, String id_chatroom_fk, String created_room, String nama_m_audience) {
        this.id_chat_participant = id_chat_participant;
        this.id_m_audience_fk = id_m_audience_fk;
        this.id_chatroom_fk = id_chatroom_fk;
        this.created_room = created_room;
        this.nama_m_audience = nama_m_audience;
    }

    public String getId_chat_participant() {
        return id_chat_participant;
    }

    public void setId_chat_participant(String id_chat_participant) {
        this.id_chat_participant = id_chat_participant;
    }

    public String getId_m_audience_fk() {
        return id_m_audience_fk;
    }

    public void setId_m_audience_fk(String id_m_audience_fk) {
        this.id_m_audience_fk = id_m_audience_fk;
    }

    public String getId_chatroom_fk() {
        return id_chatroom_fk;
    }

    public void setId_chatroom_fk(String id_chatroom_fk) {
        this.id_chatroom_fk = id_chatroom_fk;
    }

    public String getCreated_room() {
        return created_room;
    }

    public void setCreated_room(String created_room) {
        this.created_room = created_room;
    }

    public String getNama_m_audience() {
        return nama_m_audience;
    }

    public void setNama_m_audience(String nama_m_audience) {
        this.nama_m_audience = nama_m_audience;
    }

    @Override
    public String toString() {
        return "Chatparticipant{" +
                "id_chat_participant='" + id_chat_participant + '\'' +
                ", id_m_audience_fk='" + id_m_audience_fk + '\'' +
                ", id_chatroom_fk='" + id_chatroom_fk + '\'' +
                ", created_room='" + created_room + '\'' +
                ", nama_m_audience='" + nama_m_audience + '\'' +
                '}';
    }
}
