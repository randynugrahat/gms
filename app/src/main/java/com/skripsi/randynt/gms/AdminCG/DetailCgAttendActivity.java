package com.skripsi.randynt.gms.AdminCG;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.CgDetailAttendListAdapter;
import com.skripsi.randynt.gms.classDef.AttendanceCg;
import com.skripsi.randynt.gms.classDef.Audience;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetailCgAttendActivity extends AppCompatActivity {
    Bundle extra ;
    String cgattendDetail;
    private ArrayList<AttendanceCg> attendanceList;
    private String id_pertemuan_cg;
    private String nama_pertemuan;
    private String id_cg;
    private String tanggal;
    private String place;
    private String id_pemimpin;
    private String nama_pemimpin;
    private String keterangan;
    private String nama_cg;
    private String URL_PRODUCTS = "";
    private ListView mListView;
    private int[] kehadiran =new int[2];
    private String[] label = {"Hadir", "Tidak Hadir"};
    TextView pertemuan_textview ;
    TextView tanggal_textview ;
    TextView namacg_textview ;
    TextView place_textview ;
    TextView pemimpin_textview ;
    LinearLayout list_layout;
    LinearLayout chart_layout;
    ToggleButton attendBtn;
    int jumlah_kehadiran;
    int jumlah_anggota;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_cg_attend);



        extra = getIntent().getExtras();
        cgattendDetail =extra.getString("cgattenddetail");
        try {
            JSONObject jsonObject = new JSONObject(cgattendDetail);
            JSONObject cgattenddata = jsonObject.getJSONObject("CgMeeting");
            id_pertemuan_cg = cgattenddata.get("id_pertemuan_cg").toString();
            nama_pertemuan = cgattenddata.get("nama_pertemuan").toString();
            id_cg = cgattenddata.get("id_m_connectgroup").toString();
            tanggal = cgattenddata.get("tanggal").toString();
            place = cgattenddata.get("place").toString();
            id_pemimpin = cgattenddata.get("id_pemimpin").toString();
            nama_pemimpin = cgattenddata.get("nama_pemimpin").toString();
            keterangan = cgattenddata.get("keterangan").toString();
            nama_cg = cgattenddata.get("nama_connectgroup").toString();
            this.URL_PRODUCTS = "https://nxtxdev.xyz/api/cgattendance?id="+id_pertemuan_cg+"&type=1";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        pertemuan_textview=(TextView)findViewById(R.id.textView_pertemuan);
        tanggal_textview=(TextView)findViewById(R.id.textView_tanggal);
        place_textview=(TextView)findViewById(R.id.textView_place);
        namacg_textview=(TextView)findViewById(R.id.textView_namacg);
        pemimpin_textview=(TextView)findViewById(R.id.textView_pemimpin);
        mListView = (ListView)findViewById(android.R.id.list);
        list_layout = (LinearLayout)findViewById(R.id.attendlist);
        chart_layout = (LinearLayout)findViewById(R.id.chartlist);
        attendBtn = (ToggleButton) findViewById(R.id.Attendtogglebutton);

        attendBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if(attendBtn.isChecked()){
                    setLayoutVisible();
                }
                else
                {

                    setLayoutInvisible();
                }

            }
        });
        pertemuan_textview.setText(nama_pertemuan);
        tanggal_textview.setText(tanggal);
        place_textview.setText(place);
        namacg_textview.setText(nama_cg);
        pemimpin_textview.setText(nama_pemimpin);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String value= attendanceList.get(position).getId_pertemuan_cg();
                String value_audience= attendanceList.get(position).getId_m_audience();
                Intent intent = new Intent(DetailCgAttendActivity.this, EvalCgActivity.class);
                intent.putExtra("pertemuancgid", value);
                intent.putExtra("id_audience",value_audience);
                startActivity(intent);
            }
        });

//        loadEvent();
        setLayoutInvisible();


        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_add:
                        Intent intent= new Intent(DetailCgAttendActivity.this,QrAttendActivity.class);
                        intent.putExtra("idpertemuam_cg", id_pertemuan_cg);
                        startActivity(intent);
                        return true;
                }
                return false;
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadEvent();
    }
    private void setupPieChart(){
        List<PieEntry> cgAttendanceCounts = new ArrayList<>();
        for(int i =0;i<2;i++){
            cgAttendanceCounts.add(new PieEntry(kehadiran[i],label[i]));
        }
        PieDataSet dataset = new PieDataSet(cgAttendanceCounts,"Presentasi Kehadiran");
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData data = new PieData(dataset);

        PieChart chart = (PieChart)findViewById(R.id.cgChart);
        chart.setData(data);
        chart.getDescription().setEnabled(false);
        chart.invalidate();
    }
    public void setLayoutInvisible() {
        if (list_layout.getVisibility() == View.VISIBLE) {
            list_layout.setVisibility(View.GONE);
            chart_layout.setVisibility(View.VISIBLE);
        }
    }
    public void setLayoutVisible() {
        if (list_layout.getVisibility() == View.GONE) {
            list_layout.setVisibility(View.VISIBLE);
            chart_layout.setVisibility(View.GONE);

        }
    }
    private void setAdapter(ArrayList<AttendanceCg> cgAttend){
        CgDetailAttendListAdapter adapter = new CgDetailAttendListAdapter(this,R.layout.adapter_cgdetail_attend,cgAttend);
        mListView.setAdapter(adapter);
    }
    private void loadData() {

        String URL_PRODUCTS1 = "https://nxtxdev.xyz/api/";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS1+"user?id="+id_cg+"&type=3",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            jumlah_anggota=length;
                            int tidak_hadir = jumlah_anggota-jumlah_kehadiran;
                            kehadiran[1] = tidak_hadir;

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        setupPieChart();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
    private void loadEvent()
    {
        attendanceList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");
                            jumlah_kehadiran = length;
                            kehadiran[0]=jumlah_kehadiran;
                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                attendanceList.add(new AttendanceCg(
                                        cgmeet.getString("id_cg_attendance"),
                                        cgmeet.getString("id_pertemuan_connectgroup_fk"),
                                        cgmeet.getString("id_m_audience_fk"),
                                        cgmeet.getString("time_in"),
                                        cgmeet.getString("keterangan"),
                                        cgmeet.getString("nama_pertemuan"),
                                        cgmeet.getString("tanggal"),
                                        cgmeet.getString("place"),
                                        cgmeet.getString("nama_m_audience")

                                ));

                            }
                            setAdapter(attendanceList);
                            loadData();


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.e("MENU",menu.toString());
        getMenuInflater().inflate(R.menu.attend_menu, menu);
        return true;
    }
}
