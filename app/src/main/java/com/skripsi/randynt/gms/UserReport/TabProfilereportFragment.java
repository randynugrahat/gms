package com.skripsi.randynt.gms.UserReport;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.PengajaranType;
import com.skripsi.randynt.gms.classDef.PengajaranUser;
import com.skripsi.randynt.gms.classDef.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TabProfilereportFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TabProfilereportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabProfilereportFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<User> userList = new ArrayList<>();
    private ArrayList<PengajaranType> typePengajaranList = new ArrayList<>();
    private ArrayList<PengajaranUser> pengajaranUserList = new ArrayList<>();
    private int userID;
    TextView profileName;
    TextView connectgroup;
    TextView name_textview;
    TextView email_textview;
    TextView phone_textview;
    TextView birth_textview;
    TextView address_textview;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public TabProfilereportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TabProfilereportFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TabProfilereportFragment newInstance(String param1, String param2) {
        TabProfilereportFragment fragment = new TabProfilereportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_tab_profilereport, container, false);
        Intent intent = getActivity().getIntent();
        Bundle extras = intent.getExtras();
        userID=extras.getInt("userid");
        loadUserdata();
        loadPengajaran();
        for (int i = 0; i < typePengajaranList.size(); i++) {
            loadPengajaranUser(typePengajaranList.get(i).getId_pengajaran_type());
        }


        profileName=(TextView)view.findViewById(R.id.profileName);
        connectgroup=(TextView)view.findViewById(R.id.connectgroup);
        name_textview=(TextView)view.findViewById(R.id.name_textview);
        email_textview=(TextView)view.findViewById(R.id.email_textview);
        phone_textview=(TextView)view.findViewById(R.id.phone_textview);
        birth_textview=(TextView)view.findViewById(R.id.birth_textview);
        address_textview=(TextView)view.findViewById(R.id.address_textview);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void loadPengajaranUser(final String ajar) {

        String URL_PRODUCTS = "https://nxtxdev.xyz/api/pengajaranattend?id="+userID+"&idajar="+ajar;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray newsdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject type = newsdata.getJSONObject(i);
                                pengajaranUserList.add(new PengajaranUser(
                                        type.getString("id_pengajaran_attend"),
                                        type.getString("id_m_audience_fk"),
                                        type.getString("id_pengajaran_type_fk")
                                ));

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }

    private void loadPengajaran() {

        String URL_PRODUCTS = "https://nxtxdev.xyz/api/pengajarantype";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray newsdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject type = newsdata.getJSONObject(i);
                               typePengajaranList.add(new PengajaranType(
                                        type.getString("id_pengajaran_type"),
                                        type.getString("nama_pengajaran_type"),
                                        type.getString("kelulusan")
                                ));

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }


    private void loadUserdata()
    {
        String URL_PRODUCTS = "https://nxtxdev.xyz/api/user?id="+this.userID+"&type=1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");


                            JSONArray userdataarray = jsonObject.getJSONArray("data");
                            JSONObject userdata = userdataarray.getJSONObject(0);

                            userList.add(new User(
                                    userdata.getInt("id_m_audience"),
                                    userdata.getString("nama_m_audience"),
                                    userdata.getString("tanggal_lahir"),
                                    userdata.getString("alamat"),
                                    userdata.getString("email"),
                                    userdata.getString("nama_connectgroup"),
                                    userdata.getString("no_telp")
                            ));
                            Log.e("userdataList",userList.get(0).toString());
                            profileName.setText(userList.get(0).getNama_audience());
                            name_textview.setText(userList.get(0).getNama_audience());
                            connectgroup.setText(userList.get(0).getConnectgroup());
                            email_textview.setText(userList.get(0).getEmail());
                            phone_textview.setText(userList.get(0).getPhone());
                            birth_textview.setText(userList.get(0).getTgl_lahir());
                            address_textview.setText(userList.get(0).getAlamat());


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
