package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.AttendanceCg;

import java.util.List;

public class CgDetailAttendListAdapter extends ArrayAdapter<AttendanceCg> {
    private String id_cg_attendance;
    private String id_pertemuan_cg;
    private String id_m_audience;
    private String time_in;
    private String keterangan;
    private String nama_pertemuan;
    private String tanggal_pertemuan;
    private String place;
    private String nama_audience;
    private static final String TAG = "EventListAdapter";
    private Context mContext;
    int mResource;

    public CgDetailAttendListAdapter(@NonNull Context context, int resource, @NonNull List<AttendanceCg> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        id_pertemuan_cg = getItem(position).getId_pertemuan_cg();
        nama_pertemuan = getItem(position).getNama_pertemuan();
        id_cg_attendance = getItem(position).getId_cg_attendance();
        tanggal_pertemuan = getItem(position).getTanggal_pertemuan();
        place = getItem(position).getPlace();
        id_m_audience = getItem(position).getId_m_audience();
        nama_audience = getItem(position).getNama_audience();
        keterangan = getItem(position).getKeterangan();
        time_in = getItem(position).getTime_in();

        AttendanceCg attendcgMeeting = new AttendanceCg(id_cg_attendance,id_pertemuan_cg,id_m_audience,time_in,keterangan,nama_pertemuan,tanggal_pertemuan,place,nama_audience);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView TvName = (TextView) convertView.findViewById(R.id.nama_textview);
        TextView TvTime = (TextView) convertView.findViewById(R.id.timein_textview);

        // TvPicture.setText(Picture);
        TvName.setText(nama_audience);
        TvTime.setText(time_in);
        return convertView;
    }
}