package com.skripsi.randynt.gms.AdminCG;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddCgActivity extends AppCompatActivity implements OnItemSelectedListener {
    private String iduser;
    private String name_cgMeeting;
    private String tanggal;
    private String place;
    private String id_pemimpin;
    private String keterangan;
    private String created_time;
    private String id_m_audience;
    private String id_m_cg;
    private EditText name_edit;
    private EditText tanggal_edit;
    private EditText place_edit;
    private Spinner pemimpin_spinner;
    private EditText keterangan_edit;
    MaterialButton saveButton;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/";
    private List<String> userList;
    private List<Integer> userListID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cg);

        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.iduser = sharedPref.getString("userid","");

        this.id_m_cg = sharedPref.getString("userid_m_audience","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("");
        userList = new ArrayList<String>();
        userListID = new ArrayList<Integer>();
        Intent intent=getIntent();
        userList =intent.getStringArrayListExtra("pemimpindata");
        userListID =intent.getIntegerArrayListExtra("pemimpindataID");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        name_edit=  (EditText) findViewById(R.id.Nama_pertemuan);
        tanggal_edit=  (EditText) findViewById(R.id.tanggal);
        place_edit=  (EditText) findViewById(R.id.tempat);
        keterangan_edit=  (EditText) findViewById(R.id.keterangan);
        pemimpin_spinner =(Spinner)findViewById(R.id.spinner_pemimpin);



        ArrayAdapter<String> adapter=new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,userList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pemimpin_spinner.setAdapter(adapter);

        pemimpin_spinner.setOnItemSelectedListener(this);
        saveButton =findViewById(R.id.Addcg_btn);

        saveButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                name_cgMeeting=name_edit.getText().toString();
                tanggal=tanggal_edit.getText().toString();
                place=place_edit.getText().toString();
                keterangan=keterangan_edit.getText().toString();
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(c.getTime());

                addCG(id_pemimpin,formattedDate);
            }
        });


    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        this.id_pemimpin = userListID.get(pos).toString();
    }


    public void onNothingSelected(AdapterView<?> parent) {

        Toast.makeText(AddCgActivity.this,"Nothing",Toast.LENGTH_LONG);
    }

    private void addCG(final String idpemimpin,final String datenow){

        StringRequest request = new StringRequest(Request.Method.POST, URL_PRODUCTS+"pertemuancg",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject userdata = jsonObject.getJSONObject("data");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(),"Add CG Success",Toast.LENGTH_LONG).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("nama_pertemuan",name_cgMeeting);
                params.put("id_m_connectgroup",id_m_cg);
                params.put("tanggal",tanggal);
                params.put("place",place);
                params.put("id_audience",idpemimpin);
                params.put("keterangan",keterangan);
                params.put("created",datenow);
                params.put("created_id",iduser);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);



    }

}
