package com.skripsi.randynt.gms.ProfilePage;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.skripsi.randynt.gms.R;

import org.w3c.dom.Text;

public class MyqrcodeActivity extends AppCompatActivity {

    private String userID;
    private String birthdate;
    private String nama;
    private ImageView qrImage;
    private TextView nameTextview;
    private TextView birthTextview;
    private String qrURL ="https://nxtxdev.xyz/assets/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myqrcode);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","0");
        nama = sharedPref.getString("nama_audience","0");
        birthdate = sharedPref.getString("tgl_lahir","0");

        qrImage =(ImageView)findViewById(R.id.userqrcode);
        nameTextview=(TextView)findViewById(R.id.qrcodename);
        birthTextview=(TextView)findViewById(R.id.qrcodebirthdate);
        nameTextview.setText(nama);
        birthTextview.setText(birthdate);
        Glide.with(this)
                .load(qrURL+userID+".png")
                .into(qrImage);
    }
}
