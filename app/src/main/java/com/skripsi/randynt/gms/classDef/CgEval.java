package com.skripsi.randynt.gms.classDef;

public class CgEval {
    private String id_cg_eval;
    private String id_m_audience;
    private String id_pertemuan_cg;
    private String nilai_pertemuan;
    private String nilai_pemimpin;
    private String keterangan;
    private String nama_audience;
    private String nama_pertemuan;
    private String tanggal_pertemuan;

    public CgEval(String id_cg_eval, String id_m_audience, String id_pertemuan_cg, String nilai_pertemuan, String nilai_pemimpin, String keterangan, String nama_audience, String nama_pertemuan, String tanggal_pertemuan) {
        this.id_cg_eval = id_cg_eval;
        this.id_m_audience = id_m_audience;
        this.id_pertemuan_cg = id_pertemuan_cg;
        this.nilai_pertemuan = nilai_pertemuan;
        this.nilai_pemimpin = nilai_pemimpin;
        this.keterangan = keterangan;
        this.nama_audience = nama_audience;
        this.nama_pertemuan = nama_pertemuan;
        this.tanggal_pertemuan = tanggal_pertemuan;
    }

    @Override
    public String toString() {
        return "CgEval{" +
                "id_cg_eval='" + id_cg_eval + '\'' +
                ", id_m_audience='" + id_m_audience + '\'' +
                ", id_pertemuan_cg='" + id_pertemuan_cg + '\'' +
                ", nilai_pertemuan='" + nilai_pertemuan + '\'' +
                ", nilai_pemimpin='" + nilai_pemimpin + '\'' +
                ", keterangan='" + keterangan + '\'' +
                ", nama_audience='" + nama_audience + '\'' +
                ", nama_pertemuan='" + nama_pertemuan + '\'' +
                ", tanggal_pertemuan='" + tanggal_pertemuan + '\'' +
                '}';
    }

    public String getId_cg_eval() {
        return id_cg_eval;
    }

    public void setId_cg_eval(String id_cg_eval) {
        this.id_cg_eval = id_cg_eval;
    }

    public String getId_m_audience() {
        return id_m_audience;
    }

    public void setId_m_audience(String id_m_audience) {
        this.id_m_audience = id_m_audience;
    }

    public String getId_pertemuan_cg() {
        return id_pertemuan_cg;
    }

    public void setId_pertemuan_cg(String id_pertemuan_cg) {
        this.id_pertemuan_cg = id_pertemuan_cg;
    }

    public String getNilai_pertemuan() {
        return nilai_pertemuan;
    }

    public void setNilai_pertemuan(String nilai_pertemuan) {
        this.nilai_pertemuan = nilai_pertemuan;
    }

    public String getNilai_pemimpin() {
        return nilai_pemimpin;
    }

    public void setNilai_pemimpin(String nilai_pemimpin) {
        this.nilai_pemimpin = nilai_pemimpin;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNama_audience() {
        return nama_audience;
    }

    public void setNama_audience(String nama_audience) {
        this.nama_audience = nama_audience;
    }

    public String getNama_pertemuan() {
        return nama_pertemuan;
    }

    public void setNama_pertemuan(String nama_pertemuan) {
        this.nama_pertemuan = nama_pertemuan;
    }

    public String getTanggal_pertemuan() {
        return tanggal_pertemuan;
    }

    public void setTanggal_pertemuan(String tanggal_pertemuan) {
        this.tanggal_pertemuan = tanggal_pertemuan;
    }
}
