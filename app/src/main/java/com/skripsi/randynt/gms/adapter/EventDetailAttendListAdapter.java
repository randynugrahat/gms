package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.AttendanceEvent;

import java.util.List;

public class EventDetailAttendListAdapter extends ArrayAdapter<AttendanceEvent> {
    private String id_event_attendance;
    private String id_event;
    private String id_m_audience;
    private String time_in;
    private String nama_audience;
    private static final String TAG = "EventListAdapter";
    private Context mContext;
    int mResource;

    public EventDetailAttendListAdapter(@NonNull Context context, int resource, @NonNull List<AttendanceEvent> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        id_event_attendance = getItem(position).getId_event_attendance();
        id_event = getItem(position).getId_event();
        id_m_audience = getItem(position).getId_m_audience();
        time_in = getItem(position).getTime_in();
        nama_audience = getItem(position).getNama_audience();


        AttendanceEvent attendanceEvent = new AttendanceEvent(id_event_attendance,id_event,id_m_audience,time_in,nama_audience);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView TvName = (TextView) convertView.findViewById(R.id.nama_textview);
        TextView TvTime = (TextView) convertView.findViewById(R.id.timein_textview);

        // TvPicture.setText(Picture);
        TvName.setText(nama_audience);
        TvTime.setText(time_in);
        return convertView;
    }
}