package com.skripsi.randynt.gms.classDef;

public class EventVolunteer {
    private String id_event_ministry;
    private String id_m_ministry;
    private String id_event;
    private String nama_event;
    private String tanggal;
    private String keterangan;
    private String id_pengajaran_type;

    public EventVolunteer(String id_event_ministry, String id_m_ministry, String id_event, String nama_event, String tanggal, String keterangan, String id_pengajaran_type) {
        this.id_event_ministry = id_event_ministry;
        this.id_m_ministry = id_m_ministry;
        this.id_event = id_event;
        this.nama_event = nama_event;
        this.tanggal = tanggal;
        this.keterangan = keterangan;
        this.id_pengajaran_type = id_pengajaran_type;
    }

    @Override
    public String toString() {
        return "{EventVolunteer:{" +
                "'id_event_ministry':'" + id_event_ministry + '\'' +
                ", 'id_m_ministry':'" + id_m_ministry + '\'' +
                ", 'id_event':'" + id_event + '\'' +
                ", 'nama_event':'" + nama_event + '\'' +
                ", 'tanggal':'" + tanggal + '\'' +
                ", 'keterangan':'" + keterangan + '\'' +
                ", 'id_pengajaran_type':'" + id_pengajaran_type + '\'' +
                '}'+
                '}';
    }

    public String getId_event_ministry() {
        return id_event_ministry;
    }

    public void setId_event_ministry(String id_event_ministry) {
        this.id_event_ministry = id_event_ministry;
    }

    public String getId_m_ministry() {
        return id_m_ministry;
    }

    public void setId_m_ministry(String id_m_ministry) {
        this.id_m_ministry = id_m_ministry;
    }

    public String getId_event() {
        return id_event;
    }

    public void setId_event(String id_event) {
        this.id_event = id_event;
    }

    public String getNama_event() {
        return nama_event;
    }

    public void setNama_event(String nama_event) {
        this.nama_event = nama_event;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getId_pengajaran_type() {
        return id_pengajaran_type;
    }

    public void setId_pengajaran_type(String id_pengajaran_type) {
        this.id_pengajaran_type = id_pengajaran_type;
    }
}
