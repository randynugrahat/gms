package com.skripsi.randynt.gms.UserReport;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.UserEventMinistryListAdapter;
import com.skripsi.randynt.gms.classDef.EventMinistry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TabMinistryreportFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TabMinistryreportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabMinistryreportFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/eventministry?";
    private ArrayList<EventMinistry> attendeventList= new ArrayList<>();
    private int userID;
    private ListView mListView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public TabMinistryreportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TabMinistryreportFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TabMinistryreportFragment newInstance(String param1, String param2) {
        TabMinistryreportFragment fragment = new TabMinistryreportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_tab_ministryreport, container, false);
        mListView = (ListView) view.findViewById(android.R.id.list);
        Intent intent = getActivity().getIntent();
        Bundle extras = intent.getExtras();
        userID=extras.getInt("userid");
        loadEvent();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setAdapter(ArrayList<EventMinistry> event){
        UserEventMinistryListAdapter adapter = new UserEventMinistryListAdapter(getActivity().getBaseContext(),R.layout.adapter_comment_layout,event);
        mListView.setAdapter(adapter);
    }
    private void loadEvent() {

        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        String URL_PRODUCTS1=URL_PRODUCTS+"id="+userID+"&type=2";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject event = eventdata.getJSONObject(i);
                                attendeventList.add(new EventMinistry(
                                        event.getString("id_event_ministry"),
                                        event.getString("id_audience_fk"),
                                        event.getString("id_event_fk"),
                                        event.getString("id_m_ministry"),
                                        event.getString("nama"),
                                        event.getString("nama_event"),
                                        event.getString("tanggal_mulai")
                                ));

                            }

                            setAdapter(attendeventList);
                         /*   if(adapter == null){
                                adapter = new NewsRecyclerViewAdapter(newsList);
                                recyclerView.setAdapter(adapter);
                            }else{
                                adapter.notifyDataSetChanged();
                            }*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
