package com.skripsi.randynt.gms;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.skripsi.randynt.gms.AttendPage.Attend1Fragment;
import com.skripsi.randynt.gms.AttendPage.Attend2Fragment;
import com.skripsi.randynt.gms.BottomFragment.Attendance_fragment;
import com.skripsi.randynt.gms.BottomFragment.Contact_fragment;
import com.skripsi.randynt.gms.BottomFragment.Content_fragment;
import com.skripsi.randynt.gms.BottomFragment.Event_fragment;
import com.skripsi.randynt.gms.BottomFragment.Profile_fragment;
import com.skripsi.randynt.gms.contentPage.Tab1;
import com.skripsi.randynt.gms.contentPage.Tab2;
import com.skripsi.randynt.gms.contentPage.Tab3;

public class ContentActivity extends AppCompatActivity implements Content_fragment.OnFragmentInteractionListener,Contact_fragment.OnFragmentInteractionListener,Attendance_fragment.OnFragmentInteractionListener,Event_fragment.OnFragmentInteractionListener,Profile_fragment.OnFragmentInteractionListener,Tab1.OnFragmentInteractionListener,Tab2.OnFragmentInteractionListener,Tab3.OnFragmentInteractionListener,Attend1Fragment.OnFragmentInteractionListener,Attend2Fragment.OnFragmentInteractionListener {

    final Fragment fragment_content = new Content_fragment();
    final Fragment fragment_contact = new Contact_fragment();
    final Fragment fragment_attendance = new Attendance_fragment();
    final Fragment fragment_event = new Event_fragment();
    final Fragment fragment_profile = new Profile_fragment();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = fragment_content;
    private final String CHANNEL_ID = "Event Reminder";
    private final int NOTIFICATION_ID =001;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        //loadFragment(new Content_fragment());
        //recyclerView
        Intent i= new Intent(this, ScheduledService.class);
        this.startService(i);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        createNotificationChannel();
//        displayNotification();
        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        String userid = sharedPref.getString("userid","");
        Log.e("USERid",userid);
        //BottomMenu
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fm.beginTransaction().add(R.id.main_container, fragment_profile, "5").hide(fragment_profile).commit();

        fm.beginTransaction().add(R.id.main_container, fragment_event, "4").hide(fragment_event).commit();
        fm.beginTransaction().add(R.id.main_container, fragment_attendance, "3").hide(fragment_attendance).commit();
        fm.beginTransaction().add(R.id.main_container, fragment_contact, "2").hide(fragment_contact).commit();
        fm.beginTransaction().add(R.id.main_container,fragment_content, "1").commit();


        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_add:
                        Intent intent= new Intent(ContentActivity.this,QRscanActivity.class);
                        startActivity(intent);
                        return true;
                    case R.id.action_logout:
                        SharedPreferences preferences =getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();
                        Intent intent_logout = new Intent(ContentActivity.this, LoginActivity.class);
                        intent_logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent_logout);
                        return true;


                }
                return false;
            }
        });
    }

//    public void displayNotification()
//    {
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,CHANNEL_ID);
//        builder.setSmallIcon(R.drawable.logo);
//        builder.setContentTitle("Event Reminder");
//        builder.setContentText("Simple Notif");
//        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
//        Log.e("Notification","notifIN");
//        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
//        notificationManagerCompat.notify(NOTIFICATION_ID,builder.build());
//    }
    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = CHANNEL_ID;
            String description = "Event Reminder For You";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.e("MENU",menu.toString());
        getMenuInflater().inflate(R.menu.contact_list_menu_toolbar, menu);
        return true;
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()){
                case R.id.menu1:
                    fm.beginTransaction().hide(active).show(fragment_content).commit();
                    active = fragment_content;
                    return true;
                case R.id.menu2:
                    fm.beginTransaction().hide(active).show(fragment_contact).commit();
                    active = fragment_contact;
                    return true;
                case R.id.menu3:
                    fm.beginTransaction().hide(active).show(fragment_attendance).commit();
                    active = fragment_attendance;
                    return true;
                case R.id.menu4:
                    fm.beginTransaction().hide(active).show(fragment_event).commit();
                    active = fragment_event;
                    return true;
                case R.id.menu5:
                    fm.beginTransaction().hide(active).show(fragment_profile).commit();
                    active = fragment_profile;
                    return true;
            }
            return false;
        }
    };


 /*   private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }*/

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
