package com.skripsi.randynt.gms.classDef;

public class AdminMenu {
    private String icon;
    private String menudesc;
    private Class act;

    public AdminMenu(String icon, String menudesc, Class act) {
        this.icon = icon;
        this.menudesc = menudesc;
        this.act = act;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMenudesc() {
        return menudesc;
    }

    public void setMenudesc(String menudesc) {
        this.menudesc = menudesc;
    }

    public Class getAct() {
        return act;
    }

    public void setAct(Class act) {
        this.act = act;
    }

    @Override
    public String toString() {
        return "AdminMenu{" +
                "icon='" + icon + '\'' +
                ", menudesc='" + menudesc + '\'' +
                ", act=" + act +
                '}';
    }
}
