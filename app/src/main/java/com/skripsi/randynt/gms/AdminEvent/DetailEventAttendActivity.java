package com.skripsi.randynt.gms.AdminEvent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.EventDetailAttendListAdapter;
import com.skripsi.randynt.gms.classDef.AttendanceEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailEventAttendActivity extends AppCompatActivity {
    private ArrayList<AttendanceEvent> attendanceList;
    Bundle extra;
    String eventattendDetail;
    private String nama_pertemuan;
    private String keterangan;
    private String tanggal;
    private String id_event;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/eventattend?";
    TextView nama_textview ;
    TextView tanggal_textview ;
    TextView keterangan_textview ;
    private ListView mListView;
    private String id_tipe_pengajaran;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_event_attend);
        extra = getIntent().getExtras();
        eventattendDetail =extra.getString("eventattenddetail");

        try {
            JSONObject jsonObject = new JSONObject(eventattendDetail);
            JSONObject eventattenddata = jsonObject.getJSONObject("EventVolunteer");
            nama_pertemuan = eventattenddata.get("nama_event").toString();
            tanggal = eventattenddata.get("tanggal").toString();
            keterangan = eventattenddata.get("keterangan").toString();
            id_event = eventattenddata.get("id_event").toString();
            id_tipe_pengajaran = eventattenddata.get("id_pengajaran_type").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nama_textview=(TextView)findViewById(R.id.textView_pertemuan);
        tanggal_textview=(TextView)findViewById(R.id.textView_tanggal);
        keterangan_textview=(TextView)findViewById(R.id.textView_keterangan);
        nama_textview.setText(nama_pertemuan);
        tanggal_textview.setText(tanggal);
        keterangan_textview.setText(keterangan);

        mListView = (ListView)findViewById(android.R.id.list);
        loadEventAttend();

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_add:
                        Intent intent= new Intent(DetailEventAttendActivity.this,EventQrAttendActivity.class);
                        intent.putExtra("idevent", id_event);
                        intent.putExtra("idpengajaran", id_tipe_pengajaran);
                        startActivity(intent);
                        return true;
                }
                return false;
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void setAdapter(ArrayList<AttendanceEvent> cgAttend){
        EventDetailAttendListAdapter adapter = new EventDetailAttendListAdapter(this,R.layout.adapter_eventdetail_attend,cgAttend);
        mListView.setAdapter(adapter);
    }
    private void loadEventAttend()
    {
        attendanceList = new ArrayList<>();
        URL_PRODUCTS=URL_PRODUCTS+"id="+id_event+"&type=1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject meet = eventdata.getJSONObject(i);
                                attendanceList.add(new AttendanceEvent(
                                        meet.getString("id_event_attendance"),
                                        meet.getString("id_event_fk"),
                                        meet.getString("id_m_audience_fk"),
                                        meet.getString("time_in"),
                                        meet.getString("nama_m_audience")
                                ));

                            }
                            setAdapter(attendanceList);


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.e("MENU",menu.toString());
        getMenuInflater().inflate(R.menu.attend_event, menu);
        return true;
    }
}
