package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.EventMinistry;

import java.util.List;

public class UserEventMinistryListAdapter extends ArrayAdapter<EventMinistry> {
    private static final String TAG = "EventAttendListAdapter";
    private Context mContext;
    int mResource;
    public UserEventMinistryListAdapter(@NonNull Context context, int resource, @NonNull List<EventMinistry> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        String id_event_ministry = getItem(position).getId_event_ministry();
        String id_audience_fk = getItem(position).getId_audience_fk();
        String id_event_fk = getItem(position).getId_event_fk();
        String id_m_ministry_fk = getItem(position).getId_m_ministry_fk();
        String nama_ministry = getItem(position).getNama_ministry();
        String namaEvent = getItem(position).getNama_event();
        String tanggal_event = getItem(position).getTanggal_event();

        String[] arrTanggal = tanggal_event.split(" ");
        EventMinistry eventMinistry = new EventMinistry(id_event_ministry,id_audience_fk,id_event_fk,id_m_ministry_fk,nama_ministry,namaEvent,tanggal_event);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView TvName = (TextView) convertView.findViewById(R.id.commentname_textview);
        TextView TvDate = (TextView) convertView.findViewById(R.id.commentdate_textview);
        TextView TvTempat = (TextView) convertView.findViewById(R.id.commentdata_textview);

        // TvPicture.setText(Picture);
        TvName.setText(namaEvent);
        TvDate.setText(arrTanggal[0]);
        TvTempat.setText(nama_ministry);
        return convertView;
    }
}
