package com.skripsi.randynt.gms.ProfilePage;

import android.content.Intent;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EditProfileActivity extends AppCompatActivity {
    private String iduser;
    private String name;
    private String email;
    private String telp;
    private String alamat;
    private String birth;
    private String URL_PRODUCTS;
    private EditText name_edit;
    private EditText email_edit;
    private EditText telp_edit;
    private EditText alamat_edit;
    private EditText birth_edit;
    MaterialButton saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        this.URL_PRODUCTS="https://nxtxdev.xyz/api/user";

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        iduser=extras.getString("iduser");
        name=extras.getString("name");
        email=extras.getString("email");
        telp=extras.getString("telp");
        alamat=extras.getString("alamat");
        birth=extras.getString("birth");

        name_edit=  (EditText) findViewById(R.id.name);
        email_edit=  (EditText) findViewById(R.id.Email);
        telp_edit=  (EditText) findViewById(R.id.phone);
        alamat_edit=  (EditText) findViewById(R.id.alamat);
        birth_edit=  (EditText) findViewById(R.id.birthdate);

        name_edit.setText(name);
        email_edit.setText(email);
        telp_edit.setText(telp);
        alamat_edit.setText(alamat);
        birth_edit.setText(birth);

        saveButton =findViewById(R.id.saveeditprofile_btn);

        saveButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                name=name_edit.getText().toString();
                email=email_edit.getText().toString();
                telp=telp_edit.getText().toString();
                alamat=alamat_edit.getText().toString();
                birth=birth_edit.getText().toString();
               updateProfile(name,email,telp,alamat,birth);
            }
        });
    }

    private void updateProfile(String inputnama,String inputemail,String inputtelp, String inputalamat, String inputbirth){

        final String namafinal = inputnama;
        final String emailfinal = inputemail;
        final String telpfinal = inputtelp;
        final String alamatfinal = inputalamat;
        final String birthfinal = inputbirth;
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(),"Update Profile success",Toast.LENGTH_LONG).show();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),"Connection Error",Toast.LENGTH_LONG).show();
                    }
                })
        {
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id",iduser);
                params.put("name",namafinal);
                params.put("email",emailfinal);
                params.put("phone",telpfinal);
                params.put("birthdate",birthfinal);
                params.put("address",alamatfinal);
                return params;
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
}
