package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Connectgroup;

import java.util.List;

    public class ReportCgListAdapter extends ArrayAdapter<Connectgroup> {
        private static final String TAG = "ContactListAdapter";
        private Context mContext;
        int mResource;

        public ReportCgListAdapter(@NonNull Context context, int resource, @NonNull List<Connectgroup> objects) {
            super(context, resource, objects);
            this.mContext = context;
            this.mResource = resource;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            //String Picture = getItem(position).getPicture();

            String nama_connectgroup = getItem(position).getNama_connectgroup();
            String nama_pemimpin = getItem(position).getNama_pemimpin();
            String id_connectgroup = getItem(position).getId_connectgroup();
            String id_coachgroup_fk = getItem(position).getId_coachgroup_fk();
            Connectgroup connectgroup = new Connectgroup(id_connectgroup,nama_connectgroup,id_coachgroup_fk,nama_pemimpin);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource,parent,false);

             TextView Tvpemimpin =(TextView) convertView.findViewById(R.id.eventketerangan_textview);
            TextView TvName =(TextView) convertView.findViewById(R.id.eventname_textview);

             Tvpemimpin.setText(nama_pemimpin);
            TvName.setText(nama_connectgroup);
            return convertView;
        }
    }

