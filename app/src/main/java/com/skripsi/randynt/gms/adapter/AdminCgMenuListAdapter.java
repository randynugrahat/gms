package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.AdminCgMenu;

import java.util.List;

public class AdminCgMenuListAdapter  extends ArrayAdapter<AdminCgMenu> {
        private static final String TAG = "MenuListAdapter";
        private Context mContext;
        int mResource;

    public AdminCgMenuListAdapter(@NonNull Context context, int resource, @NonNull List<AdminCgMenu> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        String icon = getItem(position).getIcon();
        String menu = getItem(position).getMenudesc();
        Class act = getItem(position).getAct();
        AdminCgMenu pm = new AdminCgMenu(icon,menu,act);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        TextView Menu =(TextView) convertView.findViewById(R.id.menu_textview);
        ImageView imgIcon = (ImageView) convertView.findViewById((R.id.iconView));
        int id = mContext.getResources().getIdentifier(icon, "drawable", mContext.getPackageName());
        // TvPicture.setText(Picture);
        imgIcon.setImageResource(id);
        Menu.setText(menu);
        return convertView;
    }
}
