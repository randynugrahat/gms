package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.AttendanceCg;

import java.util.List;

public class UserCgAttendListAdapter extends ArrayAdapter<AttendanceCg> {
        private static final String TAG = "EventAttendListAdapter";
        private Context mContext;
        int mResource;
        public UserCgAttendListAdapter(@NonNull Context context, int resource, @NonNull List<AttendanceCg> objects) {
            super(context, resource, objects);
            this.mContext = context;
            this.mResource = resource;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            //String Picture = getItem(position).getPicture();

            String id_cg_attendance = getItem(position).getId_cg_attendance();
            String id_m_audience = getItem(position).getId_m_audience();
            String nama_m_audience = getItem(position).getNama_audience();
            String nama_pertemuan = getItem(position).getNama_pertemuan();
            String time_in = getItem(position).getTime_in();
            String keterangan = getItem(position).getKeterangan();
            String place = getItem(position).getPlace();
            String pertemuan_cg = getItem(position).getId_pertemuan_cg();
            String tanggal_pertemuan = getItem(position).getTanggal_pertemuan();

            AttendanceCg attendanceCg = new AttendanceCg(id_cg_attendance,pertemuan_cg,id_m_audience,time_in,keterangan,nama_pertemuan,tanggal_pertemuan,place,nama_m_audience);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);

            TextView TvName = (TextView) convertView.findViewById(R.id.eventname_textview);
            TextView TvTempat = (TextView) convertView.findViewById(R.id.eventketerangan_textview);

            // TvPicture.setText(Picture);
            TvName.setText(nama_pertemuan);
            TvTempat.setText(time_in);
            return convertView;
        }
    }
