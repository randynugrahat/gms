package com.skripsi.randynt.gms.AdminCG;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.PenilikReport.CgDetailActivity;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.ReportCgListAdapter;
import com.skripsi.randynt.gms.classDef.Connectgroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReportpenilikActivity extends AppCompatActivity {
    private ArrayList<Connectgroup> cgList = new ArrayList<>();
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/";
    private String userID;
    private String idcoach;
    ListView mListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportpenilik);
        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","0");

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras.getString("coachid") != null)
        {
            idcoach=extras.getString("coachid");
        }
        else {
            this.idcoach = sharedPref.getString("user_coach","0");
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("");
        loadData();
        mListView = (ListView)findViewById(android.R.id.list);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String value= cgList.get(position).getId_connectgroup();
                Intent intent = new Intent(ReportpenilikActivity.this, CgDetailActivity.class);

                intent.putExtra("idcg", value);
                startActivity(intent);
            }
        });
    }
    private void setAdapter(ArrayList<Connectgroup> audiences){
        ReportCgListAdapter adapter = new ReportCgListAdapter(this.getBaseContext(),R.layout.adapter_event_layout,audiences);
        mListView.setAdapter(adapter);
    }
    private void loadData() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"connectgroup?id="+idcoach+"&type=2",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray contactdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject contact = contactdata.getJSONObject(i);
                                cgList.add(new Connectgroup(
                                        contact.getString("id_connectgroup"),
                                        contact.getString("nama_connectgroup"),
                                        contact.getString("id_coachgroup_fk"),
                                        contact.getString("nama_m_audience")
                                ));

                            }
                            setAdapter(cgList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
}
