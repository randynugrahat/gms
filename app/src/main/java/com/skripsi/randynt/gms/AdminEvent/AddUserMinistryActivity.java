package com.skripsi.randynt.gms.AdminEvent;

import android.content.Intent;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddUserMinistryActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private Spinner ministry_spinner;
    private Spinner userMinistry_spinner;
    Bundle extra;
    MaterialButton saveButton;
    private List<String> ministryList = new ArrayList<String>();
    private List<Integer> ministryListID = new ArrayList<Integer>();
    private List<String> userMinistryList;
    private List<Integer> userMinistryListID;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/";
    private String idMinistry;
    private String idEvent;
    private String idAudience;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user_ministry);
        ministry_spinner =(Spinner)findViewById(R.id.spinner_ministry);
        userMinistry_spinner =(Spinner)findViewById(R.id.spinner_pelayan);
        Intent intent=getIntent();
        ministryList =intent.getStringArrayListExtra("ministrydata");
        ministryListID =intent.getIntegerArrayListExtra("ministrydataID");
        saveButton =findViewById(R.id.Addministry_btn);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        extra = getIntent().getExtras();
        idEvent =extra.getString("idevent");
        setSpinnerMinistryAdapter();
        saveButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
            addUserMinistry(idMinistry,idAudience);
            }
        });
    }
    public void setSpinnerMinistryAdapter()
    {
        ArrayAdapter<String> adapter=new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,ministryList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ministry_spinner.setAdapter(adapter);
        ministry_spinner.setOnItemSelectedListener(this);
    }
    public void setSpinnerUserMinistryAdapter()
    {
        ArrayAdapter<String> adapterUser=new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,userMinistryList);
        adapterUser.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        userMinistry_spinner.setAdapter(adapterUser);
        userMinistry_spinner.setOnItemSelectedListener(this);
    }
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        Spinner spin = (Spinner) parent;
        Spinner spin2 = (Spinner) parent;

        if (spin.getId() == R.id.spinner_ministry) {
            idMinistry = ministryListID.get(pos).toString();
            loadUserMinistry();
        }
        if (spin2.getId() == R.id.spinner_pelayan) {
            idAudience = userMinistryListID.get(pos).toString();
        }
    }
    public void onNothingSelected(AdapterView<?> parent) {

        Toast.makeText(AddUserMinistryActivity.this,"Nothing",Toast.LENGTH_LONG);
    }
    private void addUserMinistry(final String idministry,final String iduser){

        StringRequest request = new StringRequest(Request.Method.POST, URL_PRODUCTS+"eventministry",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject userdata = jsonObject.getJSONObject("data");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(),"Add User Success",Toast.LENGTH_LONG).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id_m_ministry_fk",idministry);
                params.put("id_audience_fk",iduser);
                params.put("id_event_fk",idEvent);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }
    private void loadUserMinistry()
    {
        userMinistryList = new ArrayList<String>();
        userMinistryListID = new ArrayList<Integer>();
        String URL_Place = this.URL_PRODUCTS+"userministry?id="+idMinistry;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_Place,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray userdata = jsonObject.getJSONArray("data");
                            for (int i = 0; i < length; i++) {
                                JSONObject user = userdata.getJSONObject(i);
                                userMinistryList.add(
                                        user.getString("nama_m_audience"));
                                userMinistryListID.add(user.getInt("id_m_audience_fk"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    setSpinnerUserMinistryAdapter();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        setSpinnerUserMinistryAdapter();
                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
