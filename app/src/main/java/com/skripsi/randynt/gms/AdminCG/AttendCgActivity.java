package com.skripsi.randynt.gms.AdminCG;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.CgAttendListAdapter;
import com.skripsi.randynt.gms.classDef.CgMeeting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AttendCgActivity extends AppCompatActivity {
    private ArrayList<CgMeeting> cgMeetList = new ArrayList<>();
    private String idcg;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/pertemuancg?";
    private ListView mListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attend_cg);

        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.idcg = sharedPref.getString("userid_m_audience","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("");


        mListView = (ListView)findViewById(android.R.id.list);
        loadEvent();

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String value= cgMeetList.get(position).toString();
                Intent intent = new Intent(AttendCgActivity.this, DetailCgAttendActivity.class);
                intent.putExtra("cgattenddetail", value);
                startActivity(intent);
            }
        });
    }
    private void setAdapter(ArrayList<CgMeeting> cgMeet){
        CgAttendListAdapter adapter = new CgAttendListAdapter(this,R.layout.adapter_cgattend_layout,cgMeet);
        mListView.setAdapter(adapter);
    }

    public void onSuccess(ArrayList<CgMeeting> cgMeet){
        Log.e("Data",cgMeetList.toString());
    }
    private void loadEvent()
    {
        String URL_PRODUCTS1 = URL_PRODUCTS+"id="+idcg+"&type=2";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                cgMeetList.add(new CgMeeting(
                                        cgmeet.getString("id_pertemuan_cg"),
                                        cgmeet.getString("nama_pertemuan"),
                                        cgmeet.getString("id_m_connectgroup"),
                                        cgmeet.getString("tanggal"),
                                        cgmeet.getString("place"),
                                        cgmeet.getString("id_audience"),
                                        cgmeet.getString("nama_m_audience"),
                                        cgmeet.getString("keterangan"),
                                        cgmeet.getString("nama_connectgroup")

                                ));

                            }
                            setAdapter(cgMeetList);


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
