package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.EventVolunteer;

import java.util.List;

public class EventAttendListAdapter extends ArrayAdapter<EventVolunteer> {
    private static final String TAG = "EventAttendListAdapter";
    private Context mContext;
    int mResource;
    public EventAttendListAdapter(@NonNull Context context, int resource, @NonNull List<EventVolunteer> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        String idEvent = getItem(position).getId_event();
        String id_eventMinistry = getItem(position).getId_event_ministry();
        String id_m_ministry = getItem(position).getId_m_ministry();
        String tanggal = getItem(position).getTanggal();
        String keterangan = getItem(position).getKeterangan();
        String nama_event = getItem(position).getNama_event();
        String id_pengajaran = getItem(position).getId_pengajaran_type();

        EventVolunteer eventVolunteer = new EventVolunteer(id_eventMinistry,id_m_ministry,idEvent,nama_event,tanggal,keterangan,id_pengajaran);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView TvName = (TextView) convertView.findViewById(R.id.eventname_textview);
        TextView TvTanggal = (TextView) convertView.findViewById(R.id.eventtanggal_textview);
        TextView TvTempat = (TextView) convertView.findViewById(R.id.eventplace_textview);

        // TvPicture.setText(Picture);
        TvName.setText(nama_event);
        TvTanggal.setText(tanggal);
        TvTempat.setText(keterangan);
        return convertView;
    }
}
