package com.skripsi.randynt.gms.CgReport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.AdminCG.ReportActivity;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.UserReport.UserReportActivity;
import com.skripsi.randynt.gms.adapter.ReportListAdapter;
import com.skripsi.randynt.gms.classDef.Audience;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TabMemberlistFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TabMemberlistFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabMemberlistFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match

    private ArrayList<Audience> audienceList = new ArrayList<>();
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/";
    private String userID;
    private String idcg;
    ListView mListView;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public TabMemberlistFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TabMemberlistFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TabMemberlistFragment newInstance(String param1, String param2) {
        TabMemberlistFragment fragment = new TabMemberlistFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_memberlist, container, false);
        SharedPreferences sharedPref = this.getActivity().getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","0");
        this.idcg = sharedPref.getString("userid_m_audience","0");

        loadData();
        mListView = (ListView)view.findViewById(android.R.id.list);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                int value= audienceList.get(position).getId();
                Intent intent = new Intent(getActivity(), UserReportActivity.class);
                Log.isLoggable("VALUE",value);
                intent.putExtra("userid", value);
                startActivity(intent);
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setAdapter(ArrayList<Audience> audiences){
        ReportListAdapter adapter = new ReportListAdapter(getActivity().getBaseContext(),R.layout.adapter_report_layout,audiences);
        mListView.setAdapter(adapter);
    }
    private void loadData() {


        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"user?id="+idcg+"&type=3",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray contactdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject contact = contactdata.getJSONObject(i);
                                audienceList.add(new Audience(
                                        contact.getInt("id_m_audience"),
                                        contact.getString("nama_m_audience"),
                                        contact.getString("nama_panggilan"),
                                        contact.getString("user_name"),
                                        contact.getInt("user_level"),
                                        contact.getString("sex"),
                                        contact.getString("tanggal_lahir"),
                                        contact.getString("tempat_lahir"),
                                        contact.getString("alamat"),
                                        contact.getInt("id_provinces"),
                                        contact.getInt("id_regencies"),
                                        contact.getInt("id_districts"),
                                        contact.getInt("id_villages"),
                                        contact.getString("no_telp")
                                ));

                            }

                            setAdapter(audienceList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
