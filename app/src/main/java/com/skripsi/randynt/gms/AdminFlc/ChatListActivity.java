package com.skripsi.randynt.gms.AdminFlc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.ChatListAdapter;
import com.skripsi.randynt.gms.adapter.MessageListAdapter;
import com.skripsi.randynt.gms.classDef.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ChatListActivity extends AppCompatActivity {
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/";
    private int[] id_chatroom;
    private ArrayList<Message> messageList;
    private ListView mListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        mListView = (ListView) findViewById(R.id.chatList);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        messageList = new ArrayList<>();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("");

        loadRoom();

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String value= messageList.get(position).getId_chatroom();
                Intent intent = new Intent(ChatListActivity.this, ChatPageActivity.class);
                intent.putExtra("id_chatroom", value);
                startActivity(intent);
            }
        });
    }
    private void setAdapter(ArrayList<Message> messages){
        ChatListAdapter adapter = new ChatListAdapter(this,R.layout.adapter_comment_layout,messages);
        mListView.setAdapter(adapter);
    }
    private void loadRoom() {

        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"chatroom",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");
                            id_chatroom = new int[length];
                            JSONArray commentdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject comment = commentdata.getJSONObject(i);
                                id_chatroom[i] =comment.getInt("id_chatroom");
                                loadMessage(Integer.toString(id_chatroom[i]));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
    private void loadMessage(final String chatroom) {

        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"message?id="+chatroom+"&type=2",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray commentdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject comment = commentdata.getJSONObject(i);
                                Log.e("messageData",comment.toString());
                                messageList.add(new Message(
                                        comment.getString("id_message"),
                                        comment.getString("id_chatroom_fk"),
                                        comment.getString("id_m_audience_fk"),
                                        comment.getString("nama_m_audience"),
                                        comment.getString("message"),
                                        comment.getString("status_message"),
                                        comment.getString("created_message"),
                                        comment.getString("listener")
                                ));

                            }

                            Log.e("MessageList",messageList.toString());
//                            mMessageAdapter.notifyDataSetChanged();
                            setAdapter(messageList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
}
