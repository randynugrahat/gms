package com.skripsi.randynt.gms.AdminEvent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.EventListAdapter;
import com.skripsi.randynt.gms.adapter.UserEventMinistryListAdapter;
import com.skripsi.randynt.gms.classDef.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EventListActivity extends AppCompatActivity {
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/";
    private ListView mListView;
    private String userID;
    private ArrayList<Event> eventList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);

        loadEvent();

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("");
        mListView = (ListView)findViewById(android.R.id.list);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String value= eventList.get(position).toString();
                Intent intent = new Intent(EventListActivity.this, UserEventMinistryActivity.class);
                intent.putExtra("eventDetail", value);

                startActivity(intent);
            }
        });
    }
    private void setAdapter(ArrayList<Event> cgMeet){
        EventListAdapter adapter = new EventListAdapter(this,R.layout.adapter_event_layout,cgMeet);
        mListView.setAdapter(adapter);
    }
    private void loadEvent()
    {
        String URL_PRODUCTS1 = URL_PRODUCTS+"events";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                eventList.add(new Event(
                                        cgmeet.getInt("id_event"),
                                        cgmeet.getString("nama_event"),
                                        cgmeet.getString("tanggal_mulai"),
                                        cgmeet.getString("keterangan_event"),
                                        cgmeet.getString("nama_m_place"),
                                        cgmeet.getString("alamat"),
                                        cgmeet.getDouble("longitude"),
                                        cgmeet.getDouble("latitude")

                                ));

                            }
                            setAdapter(eventList);


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }

}
