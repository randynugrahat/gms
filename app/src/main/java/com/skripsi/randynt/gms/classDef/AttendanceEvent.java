package com.skripsi.randynt.gms.classDef;

public class AttendanceEvent {
    private String id_event_attendance;
    private String id_event;
    private String id_m_audience;
    private String time_in;
    private String nama_audience;

    public AttendanceEvent(String id_event_attendance, String id_event, String id_m_audience, String time_in, String nama_audience) {
        this.id_event_attendance = id_event_attendance;
        this.id_event = id_event;
        this.id_m_audience = id_m_audience;
        this.time_in = time_in;
        this.nama_audience = nama_audience;
    }

    @Override
    public String toString() {
        return "{AttendanceEvent:{" +
                "'id_event_attendance':'" + id_event_attendance + '\'' +
                ", 'id_event':'" + id_event + '\'' +
                ", 'id_m_audience':'" + id_m_audience + '\'' +
                ", 'time_in':'" + time_in + '\'' +
                ", 'nama_audience':'" + nama_audience + '\'' +
                '}'+
                '}';
    }

    public String getId_event_attendance() {
        return id_event_attendance;
    }

    public void setId_event_attendance(String id_event_attendance) {
        this.id_event_attendance = id_event_attendance;
    }

    public String getId_event() {
        return id_event;
    }

    public void setId_event(String id_event) {
        this.id_event = id_event;
    }

    public String getId_m_audience() {
        return id_m_audience;
    }

    public void setId_m_audience(String id_m_audience) {
        this.id_m_audience = id_m_audience;
    }

    public String getTime_in() {
        return time_in;
    }

    public void setTime_in(String time_in) {
        this.time_in = time_in;
    }

    public String getNama_audience() {
        return nama_audience;
    }

    public void setNama_audience(String nama_audience) {
        this.nama_audience = nama_audience;
    }
}
