package com.skripsi.randynt.gms.GembalaReport;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Coachgroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TabGembalaReportFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TabGembalaReportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabGembalaReportFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ArrayList<Coachgroup> coachgroupList = new ArrayList<>();
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/";
    private String userID;
    private String idcabang;
    private TextView jemaatText;
    private TextView CgText;
    private TextView CoachText;
    private int[] tahunJemaat;
    private int[] tahunCG;
    private int[] jumlahJemaat;
    private int[] jumlahCG;
    LineChart jemaatChart;
    LineChart cgChart;
    ArrayList<String> xVals = new ArrayList<String>();
    ArrayList<Entry> yVals = new ArrayList<Entry>();
    ArrayList<Entry> yVals_cg = new ArrayList<Entry>();
    Integer counterSynchronized=0;
    Integer counterSynchronized_cg=0;
    public TabGembalaReportFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static TabGembalaReportFragment newInstance(String param1, String param2) {
        TabGembalaReportFragment fragment = new TabGembalaReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view =inflater.inflate(R.layout.fragment_tab_gembala_report, container, false);
        SharedPreferences sharedPref = getActivity().getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","0");
        this.idcabang = sharedPref.getString("user_cabang","0");
        jemaatText = (TextView)view.findViewById(R.id.jumlahJemaatText);
        CgText =(TextView)view.findViewById(R.id.jumlahCgText);
        CoachText = (TextView)view.findViewById(R.id.jumlahCoachText);
        jemaatChart = (LineChart)view.findViewById(R.id.jemaatGrowthchart);
        cgChart = (LineChart)view.findViewById(R.id.cgGrowthchart);
        getYearCg();
        getYearJemaat();
        loadData();
        loadCg();
        loadJemaat();
        return view;
    }

    private ArrayList<Entry> setYAxisValues(final ArrayList<Entry> yaxis,final int data,final int index){

        yaxis.add(new Entry(data, index));

        return yaxis;
    }
    private ArrayList<String> setXAxisValues(final String data){
        xVals.add(data);

        return xVals;
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setData(LineChart chartdata,final ArrayList<Entry> entryArrayList) {

        LineDataSet set1;

        // create a dataset and give it a type
        set1 = new LineDataSet(entryArrayList, "DataSet 1");
        set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        // set1.enableDashedLine(10f, 5f, 0f);
        // set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.RED);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(true);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(dataSets);
//        jemaatChart.getXAxis()

        // set data
//        for(int i =0;i<entryArrayList.size();i++){
//            Log.e("yVals",entryArrayList.get(i).toString());
//        }
        chartdata.setData(data);
        chartdata.invalidate();
    }

    private void loadData() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"coach?id="+idcabang,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String length = jsonObject.getString("total");
                            CoachText.setText(length);


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }

    private void loadCg() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"connectgroup?id="+idcabang+"&type=3",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String length = jsonObject.getString("total");
                            CgText.setText(length);


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }
    private void loadJemaat() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"user?id="+idcabang+"&type=5",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");
                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                jemaatText.setText(cgmeet.getString("count"));

                            }


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }
    private void getYearJemaat()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"user?id="+idcabang+"&type=6",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");
                            tahunJemaat=new int[length];
                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                tahunJemaat[i] =cgmeet.getInt("tahun_join");


                            }


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                        jumlahJemaat = new int[tahunJemaat.length];
                        for(int j =0 ;j<tahunJemaat.length;j++)
                        {
                            Log.e("Tahun",Integer.toString(tahunJemaat[j]));
                            getJemaatCount(tahunJemaat[j],j,tahunJemaat.length);
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
    }

    private void getYearCg()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"connectgroup?id="+idcabang+"&type=4",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");
                            tahunCG = new int[length];
                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                tahunCG[i] =cgmeet.getInt("tahun_create");


                            }
                            jumlahCG=new int[length];
                            for(int j =0 ;j<tahunCG.length;j++)
                            {
                                getCgCount(tahunCG[j],j,tahunCG.length);
                            }


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
    }
    private void getCgCount(final int tahun,final int index,final int max)
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"connectgroup?id="+idcabang+"&type=5&tahun="+tahun,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");


                            JSONObject cgmeet = eventdata.getJSONObject(0);
                            jumlahCG[index] =cgmeet.getInt("count");



                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        synchronized(counterSynchronized_cg){
                            counterSynchronized_cg += 1;
                            if(counterSynchronized_cg==max){
                                int counter = 0;
                                for (int i = 0; i < max; i++) {
                                    int sums = 0;
                                    //setXAxisValues(Integer.toString(tahunJemaat[i]));
                                    for (int j = 0; j < i + 1; j++) {
                                        sums += jumlahCG[j];
                                    }
                                    setYAxisValues(yVals_cg,i, sums);
                                    counter += 1;
                                }
                                if (counter == max) {
                                    setData(cgChart,yVals_cg);
                                }
                            }
                            else {}
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
    }

    private void getJemaatCount(final int tahun,final int index,final int max){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"user?id="+idcabang+"&type=7&tahun="+tahun,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            JSONObject cgmeet = eventdata.getJSONObject(0);
                            jumlahJemaat[index] = cgmeet.getInt("count");
//                            Log.e("Tahunn", Integer.toString(tahun));
//                            Log.e("JumlahJemaat", Integer.toString(jumlahJemaat[index]));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        synchronized(counterSynchronized){
                            counterSynchronized += 1;
                            if(counterSynchronized==max){
                                int counter = 0;
                                for (int i = 0; i < max; i++) {
                                    int sums = 0;
                                    //setXAxisValues(Integer.toString(tahunJemaat[i]));
                                    for (int j = 0; j < i + 1; j++) {
                                        sums += jumlahJemaat[j];
                                    }
                                    setYAxisValues(yVals,i, sums);
                                    counter += 1;
                                }
                                if (counter == max) {
                                    setData(jemaatChart,yVals);
                                }
                            }
                            else {}
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
