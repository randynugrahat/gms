package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.UserEventAttend;

import java.util.List;

public class UserEventAttendListAdapter extends ArrayAdapter<UserEventAttend> {
    private static final String TAG = "EventAttendListAdapter";
    private Context mContext;
    int mResource;
    public UserEventAttendListAdapter(@NonNull Context context, int resource, @NonNull List<UserEventAttend> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        String idEventAttend = getItem(position).getId_event_attendance();
        String keterangan_event = getItem(position).getKeterangan_event();
        String nama_m_audience = getItem(position).getNama_m_audience();
        String time_in = getItem(position).getTime_in();
        String tanggal_mulai = getItem(position).getTanggal_mulai();
        String nama_event = getItem(position).getNama_event();

        UserEventAttend userEventAttend = new UserEventAttend(idEventAttend,time_in,nama_m_audience,nama_event,tanggal_mulai,keterangan_event);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource, parent, false);

        TextView TvName = (TextView) convertView.findViewById(R.id.eventname_textview);
        TextView TvTempat = (TextView) convertView.findViewById(R.id.eventketerangan_textview);

        // TvPicture.setText(Picture);
        TvName.setText(nama_event);
        TvTempat.setText(time_in);
        return convertView;
    }
}
