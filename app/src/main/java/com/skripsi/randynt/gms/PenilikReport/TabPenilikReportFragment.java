package com.skripsi.randynt.gms.PenilikReport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.skripsi.randynt.gms.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TabPenilikReportFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TabPenilikReportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabPenilikReportFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/";
    LineChart memberChart;
    LineChart pertemuanChart;
    PieChart genderChart;
    private String namaCG;
    private String namaPemimpin;
    private String idcg;
    private String userID;
    private String mParam1;
    private String mParam2;
    private int[] gendersize = new int[2];
    private int[] idPertemuan;
    private int[] kehadiran;
    private  int[] tahunJemaat;
    private  int[] jumlahJemaat;
    private String[] label = {"M", "F"};
    TextView textViewnamaCG;
    TextView textViewnamaPemimpin;
    TextView textViewJumlahMember;
    TextView textViewJumlahPertemuan;
    TextView textViewNilai;
    ArrayList<Entry> yValsKehadiran = new ArrayList<Entry>();
    ArrayList<Entry> yValsJemaat = new ArrayList<Entry>();
    private View viewFragment;
    Integer counterSynchronized=0;
    Integer counterSynchronized_jemaat=0;

    private OnFragmentInteractionListener mListener;

    public TabPenilikReportFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static TabPenilikReportFragment newInstance(String param1, String param2) {
        TabPenilikReportFragment fragment = new TabPenilikReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_tab_report, container, false);
        viewFragment = view;
        SharedPreferences sharedPref = this.getActivity().getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","0");
        Intent intent = getActivity().getIntent();
        Bundle extras = intent.getExtras();
        idcg=extras.getString("idcg");
        textViewnamaCG=(TextView)view.findViewById(R.id.textView_namacg);
        textViewnamaPemimpin=(TextView)view.findViewById(R.id.textView_namapemimpin);
        textViewJumlahMember=(TextView)view.findViewById(R.id.jumlahmemberText);
        textViewJumlahPertemuan=(TextView)view.findViewById(R.id.pertemuanText);
        textViewNilai=(TextView)view.findViewById(R.id.nilaiText);
        pertemuanChart=(LineChart)view.findViewById(R.id.cgAttendchart);
        memberChart=(LineChart)view.findViewById(R.id.cgMemberchart);
        loadCg();
        loadTotalMember();
        loadTotalPertemuan();
        loadAvgScore();
        loadPertemuan();
        getYearJemaat();
        for(int i=0;i<label.length;i++)
        {
            loadGender(label[i],i);
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private void setupPieChart(){
        List<PieEntry> genderCounts = new ArrayList<>();
        for(int i =0;i<2;i++){
            genderCounts.add(new PieEntry(gendersize[i],label[i]));
        }
        PieDataSet dataset = new PieDataSet(genderCounts,"Presentasi Pria/Wanita");
        dataset.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData data = new PieData(dataset);

        genderChart = (PieChart)viewFragment.findViewById(R.id.cgChart);
        genderChart.setData(data);
        genderChart.getDescription().setEnabled(false);
        genderChart.invalidate();
    }
    private ArrayList<Entry> setYAxisValues(final ArrayList<Entry> yaxis, final int data, final int index){

        yaxis.add(new Entry(data, index));

        return yaxis;
    }
    private void setData(LineChart chartdata,final ArrayList<Entry> entryArrayList) {

        LineDataSet set1;

        // create a dataset and give it a type
        set1 = new LineDataSet(entryArrayList, "DataSet 1");
        set1.setFillAlpha(110);
        // set1.setFillColor(Color.RED);

        // set the line to be drawn like this "- - - - - -"
        // set1.enableDashedLine(10f, 5f, 0f);
        // set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.RED);
        set1.setCircleColor(Color.BLACK);
        set1.setLineWidth(1f);
        set1.setCircleRadius(3f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(9f);
        set1.setDrawFilled(true);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(dataSets);
//        jemaatChart.getXAxis()

        // set data
//        for(int i =0;i<entryArrayList.size();i++){
//            Log.e("yVals",entryArrayList.get(i).toString());
//        }
        chartdata.setData(data);
        chartdata.invalidate();
    }
    private void loadCg() {


        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"connectgroup?id="+idcg+"&type=1",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray contactdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject contact = contactdata.getJSONObject(i);
                                namaCG = contact.getString("nama_connectgroup");
                                namaPemimpin = contact.getString("nama_m_audience");
                                textViewnamaCG.setText(namaCG);
                                textViewnamaPemimpin.setText(namaPemimpin);
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }
    private void loadTotalMember() {


        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"user?id="+idcg+"&type=2",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");
                            textViewJumlahMember.setText(Integer.toString(length));




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }
    private void loadTotalPertemuan() {


        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"pertemuancg?id="+idcg+"&type=2",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");
                            textViewJumlahPertemuan.setText(Integer.toString(length));




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }
    private void loadAvgScore() {


        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"cgevaluation?id="+idcg+"&type=2",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray contactdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject contact = contactdata.getJSONObject(i);

                                textViewNilai.setText(contact.getString("avg_pertemuan"));
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }
    private void loadGender(final String gender,final int index) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"user?id="+idcg+"&sex="+gender+"&type=8",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray contactdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject contact = contactdata.getJSONObject(i);

                                gendersize[index]=contact.getInt("countsex");
                            }
                            Log.e("gender0",Integer.toString(gendersize[0]));
                            Log.e("gender1",Integer.toString(gendersize[1]));
                        }

                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(gendersize[0] != 0 && gendersize[1] != 0) {
                            Log.e("Pie","PieChart");
                            setupPieChart();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
    }
    private void loadPertemuan() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"pertemuancg?id="+idcg+"&type=3",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");
                            idPertemuan = new int[length];
                            JSONArray contactdata = jsonObject.getJSONArray("data");

                            for (int i = length-1; i >=0; i--) {

                                JSONObject contact = contactdata.getJSONObject(i);

                                idPertemuan[i]=contact.getInt("id_pertemuan_cg");
                            }
                            kehadiran = new int[length];
                            for(int j =0 ;j<idPertemuan.length;j++)
                            {
                                getKehadiran(idPertemuan[j],j,idPertemuan.length);
                            }

                        }

                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
    }

    private void getKehadiran(final int id_pertemuan,final int index,final int max)
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"cgattendance?id="+id_pertemuan+"&type=3",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");


                            JSONObject cgmeet = eventdata.getJSONObject(0);
                            kehadiran[index] =cgmeet.getInt("count_pertemuan");



                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                        synchronized(counterSynchronized){
                            counterSynchronized += 1;
                            if(counterSynchronized==max){
                                int counter = 0;
                                for (int i = 0; i < max; i++) {

                                    setYAxisValues(yValsKehadiran,i, kehadiran[i]);
                                    counter += 1;
                                }
                                if (counter == max) {
                                    setData(pertemuanChart,yValsKehadiran);
                                }
                            }
                            else {}
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
    }
    private void getYearJemaat()
    {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"user?id="+idcg+"&type=10",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");
                            tahunJemaat=new int[length];
                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                tahunJemaat[i] =cgmeet.getInt("tahun_join");


                            }


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }

                        jumlahJemaat = new int[tahunJemaat.length];
                        for(int j =0 ;j<tahunJemaat.length;j++)
                        {
                            Log.e("Tahun",Integer.toString(tahunJemaat[j]));
                            getJemaatCount(tahunJemaat[j],j,tahunJemaat.length);
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
    }

    private void getJemaatCount(final int tahun,final int index,final int max){

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"user?id="+idcg+"&type=9&tahun="+tahun,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            JSONObject cgmeet = eventdata.getJSONObject(0);
                            jumlahJemaat[index] = cgmeet.getInt("count");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        synchronized(counterSynchronized_jemaat){
                            counterSynchronized_jemaat += 1;
                            if(counterSynchronized_jemaat==max){
                                int counter = 0;
                                for (int i = 0; i < max; i++) {
                                    int sums = 0;
                                    //setXAxisValues(Integer.toString(tahunJemaat[i]));
                                    for (int j = 0; j < i + 1; j++) {
                                        sums += jumlahJemaat[j];
                                    }
                                    setYAxisValues(yValsJemaat,i, sums);
                                    counter += 1;
                                }
                                if (counter == max) {
                                    setData(memberChart,yValsJemaat);
                                }
                            }
                            else {}
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
