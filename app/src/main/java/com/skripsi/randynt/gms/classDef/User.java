package com.skripsi.randynt.gms.classDef;

public class User {
    private int id_audience;
    private String nama_audience;
    private String tgl_lahir;
    private String alamat;
    private String email;
    private String connectgroup;
    private String phone;



    public User(int id_audience, String nama_audience, String tgl_lahir, String alamat, String email, String connectgroup, String phone) {
        this.id_audience = id_audience;
        this.nama_audience = nama_audience;
        this.tgl_lahir = tgl_lahir;
        this.alamat = alamat;
        this.email = email;
        this.connectgroup = connectgroup;
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId_audience() {
        return id_audience;
    }

    public void setId_audience(int id_audience) {
        this.id_audience = id_audience;
    }

    public String getNama_audience() {
        return nama_audience;
    }

    public void setNama_audience(String nama_audience) {
        this.nama_audience = nama_audience;
    }

    public String getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(String tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConnectgroup() {
        return connectgroup;
    }

    public void setConnectgroup(String connectgroup) {
        this.connectgroup = connectgroup;
    }

    @Override
    public String toString() {
        return "User{" +
                "id_audience=" + id_audience +
                ", nama_audience='" + nama_audience + '\'' +
                ", tgl_lahir='" + tgl_lahir + '\'' +
                ", alamat='" + alamat + '\'' +
                ", email='" + email + '\'' +
                ", connectgroup='" + connectgroup + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
