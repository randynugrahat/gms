package com.skripsi.randynt.gms.classDef;

public class PengajaranType {
    private String id_pengajaran_type;
    private String nama_pengajaran_type;
    private String kelulusan;

    public PengajaranType(String id_pengajaran_type, String nama_pengajaran_type, String kelulusan) {
        this.id_pengajaran_type = id_pengajaran_type;
        this.nama_pengajaran_type = nama_pengajaran_type;
        this.kelulusan = kelulusan;
    }

    public String getId_pengajaran_type() {
        return id_pengajaran_type;
    }

    public void setId_pengajaran_type(String id_pengajaran_type) {
        this.id_pengajaran_type = id_pengajaran_type;
    }

    public String getNama_pengajaran_type() {
        return nama_pengajaran_type;
    }

    public void setNama_pengajaran_type(String nama_pengajaran_type) {
        this.nama_pengajaran_type = nama_pengajaran_type;
    }

    public String getKelulusan() {
        return kelulusan;
    }

    public void setKelulusan(String kelulusan) {
        this.kelulusan = kelulusan;
    }

    @Override
    public String toString() {
        return "PengajaranType{" +
                "id_pengajaran_type='" + id_pengajaran_type + '\'' +
                ", nama_pengajaran_type='" + nama_pengajaran_type + '\'' +
                ", kelulusan='" + kelulusan + '\'' +
                '}';
    }
}
