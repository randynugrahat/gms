package com.skripsi.randynt.gms.contentPage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.CustomItemClickListener;
import com.skripsi.randynt.gms.ContentPageTab1.DetailNewsActivity;
import com.skripsi.randynt.gms.adapter.NewsRecyclerViewAdapter;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.News;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Tab1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Tab1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Tab1 extends Fragment {
    private static final String URL_PRODUCTS = "https://nxtxdev.xyz/api/news";
    private ArrayList<News> newsList;
    private RecyclerView recyclerView;
    private NewsRecyclerViewAdapter adapter;
    private OnFragmentInteractionListener mListener;
    private LayoutInflater class_inflater;
    private ViewGroup class_container;
    private Bundle class_savedInstanceState;
    private View class_view;
    private Activity act;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public Tab1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Tab1.
     */
    // TODO: Rename and change types and number of parameters
    public static Tab1 newInstance(String param1, String param2) {
        Tab1 fragment = new Tab1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void view_generator(LayoutInflater inflater, ViewGroup container,
                        Bundle savedInstanceState)
    {

        RecyclerView recyclerView = (RecyclerView) this.class_view

                .findViewById(R.id.recycler_view);
        Log.e("Oncreateview","OncreateView");
        recyclerView.setLayoutManager(new LinearLayoutManager(

                act.getBaseContext()));
        adapter = new NewsRecyclerViewAdapter(act, newsList, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                String newsdata = newsList.get(position).toString();
                Log.e("newsdetail",newsdata);
                Intent intent= new Intent(getActivity(),DetailNewsActivity.class);
                intent.putExtra("newsdetail", newsdata);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tab1, container, false);
        this.class_view = view;
        this.class_inflater= inflater;
        this.class_container = container;
        this.class_savedInstanceState= savedInstanceState;
        this.act =getActivity();
//        loadNews();
        final SwipeRefreshLayout pullToRefresh = view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadNews();
                pullToRefresh.setRefreshing(false);
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadNews();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    //GET NEWS from API
    private void loadNews() {

       /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        newsList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray newsdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject news = newsdata.getJSONObject(i);
                                newsList.add(new News(
                                        news.getInt("id"),
                                        news.getString("tanggal"),
                                        "http://admin.nxtxdev.xyz/content-news/image?name="+news.getString("image_url"),
                                        news.getInt("id_m_content_jenis_news"),
                                        news.getString("nama"),
                                        news.getString("keterangan")

                                ));
                               Log.e("News name: ",   newsList.get(i).toString());


                            }


                         /*   if(adapter == null){
                                adapter = new NewsRecyclerViewAdapter(newsList);
                                recyclerView.setAdapter(adapter);
                            }else{
                                adapter.notifyDataSetChanged();
                            }*/
                         view_generator(class_inflater,class_container,class_savedInstanceState);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
        Log.e("LoadNews","LoadNews");
//     newsList.add(new News(1,"25 Sept 2018","https://geniusquotes.org/wp-content/uploads/2014/06/Church-quote-picture.jpg",1,"Kingdom Financial Camp","<p data-tag=\\\"input\\\">Tujuan Keuangan Ilahi untuk mencapai kemakmuran yang sejati</p>"));
        int listSize = newsList.size();
        Log.isLoggable("Size",listSize);
        for (int i = 0; i<listSize; i++){
            Log.i("Member name: ", newsList.get(i).toString());
        }
    }
}
