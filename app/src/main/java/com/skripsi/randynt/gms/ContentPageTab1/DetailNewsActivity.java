package com.skripsi.randynt.gms.ContentPageTab1;

import android.content.Context;
import android.content.Intent;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.skripsi.randynt.gms.R;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailNewsActivity extends AppCompatActivity {
    Bundle extra ;
    private  String id;
    private String tanggal;
    private String image;
    private String nama_news;
    private String keterangan;
    private TextView news_title;
    private TextView content;
    private Toolbar toolbar;
    private Context mCtx;
    private ImageView newsimage;
    MaterialButton commentButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mCtx = this;
        news_title=(TextView)findViewById(R.id.newsdetailtitle);
        content =(TextView)findViewById(R.id.newsketerangan);
        //toolbar = (Toolbar) findViewById(R.id.app_bar);
        newsimage = (ImageView) findViewById(R.id.newsImage);
        commentButton = (MaterialButton) findViewById(R.id.comments_btn);
        extra = getIntent().getExtras();
        String newsExtras =extra.getString("newsdetail");
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(newsExtras);
            JSONObject newsdata = jsonObject.getJSONObject("News");
            Log.e("NEWSdata",newsdata.toString());
            news_title.setText(newsdata.get("Nama").toString());
            content.setText(newsdata.get("Keterangan").toString());
            id = newsdata.get("Id").toString();
            Glide.with(mCtx)
                    .load(newsdata.get("Image").toString())
                    .into(newsimage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        commentButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent intent= new Intent(DetailNewsActivity.this,CommentActivity.class);
                intent.putExtra("newsid", id);
                startActivity(intent);
            }
        });
    }
}
