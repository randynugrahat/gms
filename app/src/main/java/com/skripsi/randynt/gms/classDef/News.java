package com.skripsi.randynt.gms.classDef;


public class News {
    private int Id;
    private String Tanggal;
    private String Image;
    private int Id_jenis_news;
    private String Nama;
    private String Keterangan;

    public News(int id, String tanggal, String image, int id_jenis_news, String nama, String keterangan) {
        this.Id = id;
        this.Tanggal = tanggal;
        this.Image = image;
        this.Id_jenis_news = id_jenis_news;
        Nama = nama;
        Keterangan = keterangan;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTanggal() {
        return Tanggal;
    }

    public void setTanggal(String tanggal) {
        Tanggal = tanggal;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public int getId_jenis_news() {
        return Id_jenis_news;
    }

    public void setId_jenis_news(int id_jenis_news) {
        Id_jenis_news = id_jenis_news;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getKeterangan() {
        return Keterangan;
    }

    public void setKeterangan(String keterangan) {
        Keterangan = keterangan;
    }

    @Override
    public String toString() {
        return "{News:{" +
                "'Id':" + Id +
                ", 'Tanggal':'" + Tanggal + '\'' +
                ", 'Image':'" + Image + '\'' +
                ", 'Id_jenis_news':" + Id_jenis_news +
                ", 'Nama':'" + Nama + '\'' +
                ", 'Keterangan':'" + Keterangan + '\'' +
                '}'+
                '}';
    }
}