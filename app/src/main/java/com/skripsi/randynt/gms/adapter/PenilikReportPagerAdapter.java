package com.skripsi.randynt.gms.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.skripsi.randynt.gms.PenilikReport.TabAttendReportFragment;
import com.skripsi.randynt.gms.PenilikReport.TabMemberReportFragment;
import com.skripsi.randynt.gms.PenilikReport.TabPenilikReportFragment;

public class PenilikReportPagerAdapter extends FragmentStatePagerAdapter {

        int mNoOfTabs;

        public PenilikReportPagerAdapter(FragmentManager fm, int NumberOfTabs)
        {
            super(fm);
            this.mNoOfTabs = NumberOfTabs;
        }


        @Override
        public Fragment getItem(int position) {
            switch(position)
            {

                case 0:
                    TabPenilikReportFragment tab1 = new TabPenilikReportFragment();
                    return  tab1;
                case 1:
                    TabAttendReportFragment tab2 = new TabAttendReportFragment();
                    return  tab2;
                case 2:
                    TabMemberReportFragment tab3 = new TabMemberReportFragment();
                    return  tab3;
                    default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNoOfTabs;
        }
    }
