package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Message;

import java.util.List;

    public class ChatListAdapter extends ArrayAdapter<Message> {
        private static final String TAG = "CommentListAdapter";
        private Context mContext;
        int mResource;

        public ChatListAdapter(@NonNull Context context, int resource, @NonNull List<Message> objects) {
            super(context, resource, objects);
            this.mContext = context;
            this.mResource = resource;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            //String Picture = getItem(position).getPicture();

            String id_message = getItem(position).getId_message();
            String chatroom = getItem(position).getId_chatroom();
            String id_sender = getItem(position).getId_sender();
            String sender_name = getItem(position).getSender_name();
            String message = getItem(position).getMessage();
            String status_message = getItem(position).getStatus_message();
            String time = getItem(position).getTime();
            String listener = getItem(position).getListener();
            String datesplit[] = time.split(" ");
            String timesplit[] = datesplit[1].split(":");
            Message messages= new Message(id_message,chatroom,id_sender,sender_name,message,status_message,time,listener);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource,parent,false);

            // TextView TvPicture =(TextView) convertView.findViewById(R.id.name_textview);
            TextView UserName =(TextView) convertView.findViewById(R.id.commentname_textview);
            TextView Commentdata =(TextView) convertView.findViewById(R.id.commentdata_textview);

            TextView date_textview =(TextView) convertView.findViewById(R.id.commentdate_textview);

            UserName.setText(sender_name);
            Commentdata.setText(message);
            date_textview.setText(timesplit[0]+":"+timesplit[1]);
            return convertView;
        }

    }