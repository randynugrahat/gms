package com.skripsi.randynt.gms.classDef;

public class Chatroom {
    private int id_chatroom;
    private int status_chatroom;
    private String date;

    public Chatroom(int id_chatroom, int status_chatroom, String date) {
        this.id_chatroom = id_chatroom;
        this.status_chatroom = status_chatroom;
        this.date = date;
    }

    public int getId_chatroom() {
        return id_chatroom;
    }

    public void setId_chatroom(int id_chatroom) {
        this.id_chatroom = id_chatroom;
    }

    public int getStatus_chatroom() {
        return status_chatroom;
    }

    public void setStatus_chatroom(int status_chatroom) {
        this.status_chatroom = status_chatroom;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Chatroom{" +
                "id_chatroom=" + id_chatroom +
                ", status_chatroom=" + status_chatroom +
                ", date='" + date + '\'' +
                '}';
    }
}
