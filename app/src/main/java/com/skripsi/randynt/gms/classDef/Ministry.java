package com.skripsi.randynt.gms.classDef;

public class Ministry {
    private String id_m_ministry;
    private String nama;
    private String keterangan;

    public Ministry(String id_m_ministry, String nama, String keterangan) {
        this.id_m_ministry = id_m_ministry;
        this.nama = nama;
        this.keterangan = keterangan;
    }

    @Override
    public String toString() {
        return "Ministry{" +
                "id_m_ministry='" + id_m_ministry + '\'' +
                ", nama='" + nama + '\'' +
                ", keterangan='" + keterangan + '\'' +
                '}';
    }

    public String getId_m_ministry() {
        return id_m_ministry;
    }

    public void setId_m_ministry(String id_m_ministry) {
        this.id_m_ministry = id_m_ministry;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
