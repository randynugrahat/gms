package com.skripsi.randynt.gms.AdminEvent;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddEventActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private EditText name_edit;
    private EditText tanggal_edit;
    private EditText detail_edit;
    private Spinner place_spinner;
    private Spinner type_spinner;
    private String name;
    private String date;
    private String detail;
    private String iduser;
    private String idplace;
    private String idtype;
    private List<String> placeList;
    private List<Integer> placeListID;
    private List<String> eventypeList;
    private List<Integer> eventypeListID;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/";
    MaterialButton saveButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        placeList = new ArrayList<String>();
        placeListID = new ArrayList<Integer>();
        eventypeList = new ArrayList<String>();
        eventypeListID = new ArrayList<Integer>();

        Intent intent=getIntent();
        placeList =intent.getStringArrayListExtra("placedata");
        placeListID =intent.getIntegerArrayListExtra("placedataID");
        eventypeList =intent.getStringArrayListExtra("eventypedata");
        eventypeListID =intent.getIntegerArrayListExtra("eventypedataID");

        name_edit=  (EditText) findViewById(R.id.eventname_textview);
        tanggal_edit=  (EditText) findViewById(R.id.eventtanggal_textview);
        detail_edit=  (EditText) findViewById(R.id.event_keterangan_textview);
        place_spinner =(Spinner)findViewById(R.id.spinner_place);
        type_spinner =(Spinner)findViewById(R.id.spinner_type);
        saveButton =findViewById(R.id.saveevent_btn);

        ArrayAdapter<String> adapter_place=new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,placeList);
        adapter_place.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        place_spinner.setAdapter(adapter_place);
        place_spinner.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter_eventype=new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,eventypeList);
        adapter_eventype.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type_spinner.setAdapter(adapter_eventype);
        type_spinner.setOnItemSelectedListener(this);

        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.iduser = sharedPref.getString("userid","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                name=name_edit.getText().toString();
                detail=detail_edit.getText().toString();
                date=tanggal_edit.getText().toString();
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(c.getTime());
                addEvent(idplace,idtype,formattedDate);

            }
        });
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        Spinner spin = (Spinner) parent;
        Spinner spin2 = (Spinner) parent;

        if (spin.getId() == R.id.spinner_place) {
            idplace = placeListID.get(pos).toString();
        }
        if (spin2.getId() == R.id.spinner_type) {
            idtype = eventypeListID.get(pos).toString();
        }
    }

    public void onNothingSelected(AdapterView<?> parent) {

        Toast.makeText(AddEventActivity.this,"Nothing",Toast.LENGTH_LONG);
    }
    private void addEvent(final String idplace,final String idtype,final String datenow){

        StringRequest request = new StringRequest(Request.Method.POST, URL_PRODUCTS+"events",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject userdata = jsonObject.getJSONObject("data");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(),"Add Event Success",Toast.LENGTH_LONG).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("nama_event",name);
                params.put("id_m_event_type",idtype);
                params.put("tanggal_mulai",date);
                params.put("id_m_place",idplace);
                params.put("keterangan_event",detail);
                params.put("created",datenow);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }

}
