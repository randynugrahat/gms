package com.skripsi.randynt.gms.BottomFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.ContactPage.DetailContactActivity;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.ContactListAdapter;
import com.skripsi.randynt.gms.classDef.Contact;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Contact_fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Contact_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Contact_fragment extends ListFragment {
    private String URL_PRODUCTS = "";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String userID;
    private ArrayList<Contact> contactList;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ListView mListView;

    private OnFragmentInteractionListener mListener;

    public Contact_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Contact_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Contact_fragment newInstance(String param1, String param2) {
        Contact_fragment fragment = new Contact_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        contactList = new ArrayList<>();
        loadContact();
    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
//        Object obj = global_list.getAdapter().getItem(position);
        int value= contactList.get(position).getId_m_audience2();
//            Log.e("Contvalue",contactList.get(position).toString());
        Intent intent= new Intent(getActivity(),DetailContactActivity.class);
        intent.putExtra("contactdetail", value);
        startActivity(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_contact_fragment, container, false);
       mListView = (ListView) view.findViewById(android.R.id.list);
        SharedPreferences sharedPref = getActivity().getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","");

        this.URL_PRODUCTS = "https://nxtxdev.xyz/api/contact?id="+this.userID;

       return view;
    }

    public void SetAdapter()
    {
        ContactListAdapter adapter = new ContactListAdapter(getActivity().getBaseContext(),R.layout.adapter_contact_layout,contactList);
        mListView.setAdapter(adapter);
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    private void loadContact() {

        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray contactdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject contact = contactdata.getJSONObject(i);
                                contactList.add(new Contact(
                                        contact.getInt("id"),
                                        contact.getInt("id_m_audience1"),
                                        contact.getInt("id_m_audience2"),
                                        contact.getString("nama_m_audience"),
                                        contact.getString("nama_panggilan"),
                                        contact.getString("no_telp"),
                                        contact.getString("no_telp2"),
                                        contact.getString("email"),
                                        contact.getString("alamat")
                                        ));
                                  Log.e("Contact namE: ",   contactList.get(i).toString());


                            }

                            SetAdapter();
                         /*   if(adapter == null){
                                adapter = new NewsRecyclerViewAdapter(newsList);
                                recyclerView.setAdapter(adapter);
                            }else{
                                adapter.notifyDataSetChanged();
                            }*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
        Log.e("LoadNews1","LoadNews1");
//        int listSize = contactList.size();
//        Log.isLoggable("Size",contactList.size());
//        for (int i = 0; i<listSize; i++){
//            Log.e("Member name: ", contactList.get(i).toString());
//        }
    }
}
