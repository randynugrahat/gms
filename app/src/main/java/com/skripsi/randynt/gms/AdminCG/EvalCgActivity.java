package com.skripsi.randynt.gms.AdminCG;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.CgEval;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class EvalCgActivity extends AppCompatActivity {
    Bundle extra ;
    private TextView nama;
    private TextView nilai_cg;
    private TextView nilai_pemimpin;
    private TextView keterangan;
    private ArrayList<CgEval> cgEvals = new ArrayList<>();
    private String idcg;
    private String iduser;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/cgevaluation?";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eval_cg);


        extra = getIntent().getExtras();
        idcg =extra.getString("pertemuancgid");
        iduser=extra.getString("id_audience");


        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("");
        nama=  (TextView) findViewById(R.id.nama_eval_textview);
        nilai_cg=  (TextView) findViewById(R.id.nilaipertemuan_eval_textview);
        nilai_pemimpin=  (TextView) findViewById(R.id.nilaipemimpin_eval_textview);
        keterangan=  (TextView) findViewById(R.id.keterangan_eval_textview);

        loadEval();
    }

    private void loadEval()
    {
        String URL_PRODUCTS1 = URL_PRODUCTS+"id="+iduser+"&idpertemuan="+idcg+"&type=1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                cgEvals.add(new CgEval(
                                        cgmeet.getString("id_cg_eval"),
                                        cgmeet.getString("id_m_audience_fk"),
                                        cgmeet.getString("id_pertemuan_cg_fk"),
                                        cgmeet.getString("nilai_pertemuan"),
                                        cgmeet.getString("nilai_pemimpin"),
                                        cgmeet.getString("keterangan_evaluation"),
                                        cgmeet.getString("nama_m_audience"),
                                        cgmeet.getString("nama_pertemuan"),
                                        cgmeet.getString("tanggal")

                                ));
                                Log.e("cgmeet",cgEvals.get(0).toString());
                            }
                        nama.setText(cgEvals.get(0).getNama_pertemuan());
                            nilai_cg.setText(cgEvals.get(0).getNilai_pertemuan());
                            nilai_pemimpin.setText(cgEvals.get(0).getNilai_pemimpin());
                            keterangan.setText(cgEvals.get(0).getKeterangan());

                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
