package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Contact;

import java.util.List;

public class ContactListAdapter extends ArrayAdapter<Contact> {
    private static final String TAG = "ContactListAdapter";
    private Context mContext;
    int mResource;

    public ContactListAdapter(@NonNull Context context, int resource, @NonNull List<Contact> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        int idContact = getItem(position).getId_contact();
        int id_m_audience1 = getItem(position).getId_m_audience1();
        int id_m_audience2 = getItem(position).getId_m_audience2();
        String nama = getItem(position).getNama();
        String nama_panggilan = getItem(position).getNama_panggilan();
        String no_telp = getItem(position).getNo_telp();
        String no_telp2 = getItem(position).getNo_telp2();
        String email = getItem(position).getEmail();
        String alamat = getItem(position).getAlamat();
        Contact contact = new Contact(idContact,id_m_audience1,id_m_audience2,nama,nama_panggilan,no_telp,no_telp2,email,alamat);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        // TextView TvPicture =(TextView) convertView.findViewById(R.id.name_textview);
        TextView TvName =(TextView) convertView.findViewById(R.id.fullname_textview);
        TextView TvPhone =(TextView) convertView.findViewById(R.id.phone_textview);

        // TvPicture.setText(Picture);
        TvName.setText(nama);
        TvPhone.setText(no_telp);
        return convertView;
    }
}
