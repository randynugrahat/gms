package com.skripsi.randynt.gms.GembalaReport;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Connectgroup;
import com.skripsi.randynt.gms.classDef.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class GembalaReportActivity extends AppCompatActivity {
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/";
    private String userID;
    private String idcoach;
    private int total_cg;
    private int total_jemaat;
    private ArrayList<User> userList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gembala_report);
        loadData();
        loadJemaat();
    }


    private void loadJemaat() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"user?id="+idcoach+"&type=4",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray contactdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject contact = contactdata.getJSONObject(i);
                                userList.add(new User(
                                        contact.getInt("id_m_audience"),
                                        contact.getString("nama_m_audience"),
                                        contact.getString("tanggal_lahir"),
                                        contact.getString("alamat"),
                                        contact.getString("email"),
                                        contact.getString("id_m_connectgroup"),
                                        contact.getString("no_telp")
                                ));

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void loadData() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"connectgroup?id="+idcoach+"&type=2",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");
                            total_cg = length;
                            JSONArray contactdata = jsonObject.getJSONArray("data");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
}
