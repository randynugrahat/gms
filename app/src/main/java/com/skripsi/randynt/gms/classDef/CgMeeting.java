package com.skripsi.randynt.gms.classDef;

public class CgMeeting {
    private String id_pertemuan_cg;
    private String nama_pertemuan;
    private  String id_m_connectgroup;
    private String tanggal;
    private String place;
    private String id_pemimpin;
    private String nama_pemimpin;
    private String keterangan;
    private String nama_connectgroup;

    public CgMeeting(String id_pertemuan_cg, String nama_pertemuan, String id_m_connectgroup, String tanggal, String place, String id_pemimpin, String nama_pemimpin, String keterangan, String nama_connectgroup) {
        this.id_pertemuan_cg = id_pertemuan_cg;
        this.nama_pertemuan = nama_pertemuan;
        this.id_m_connectgroup = id_m_connectgroup;
        this.tanggal = tanggal;
        this.place = place;
        this.id_pemimpin = id_pemimpin;
        this.nama_pemimpin = nama_pemimpin;
        this.keterangan = keterangan;
        this.nama_connectgroup = nama_connectgroup;
    }

    @Override
    public String toString() {
        return "{CgMeeting:{" +
                "'id_pertemuan_cg':'" + id_pertemuan_cg + '\'' +
                ", 'nama_pertemuan':'" + nama_pertemuan + '\'' +
                ", 'id_m_connectgroup':'" + id_m_connectgroup + '\'' +
                ", 'tanggal':'" + tanggal + '\'' +
                ", 'place':'" + place + '\'' +
                ", 'id_pemimpin':'" + id_pemimpin + '\'' +
                ", 'nama_pemimpin':'" + nama_pemimpin + '\'' +
                ", 'keterangan':'" + keterangan + '\'' +
                ", 'nama_connectgroup':'" + nama_connectgroup + '\'' +
                '}'+
                '}';
    }

    public String getId_pertemuan_cg() {
        return id_pertemuan_cg;
    }

    public void setId_pertemuan_cg(String id_pertemuan_cg) {
        this.id_pertemuan_cg = id_pertemuan_cg;
    }

    public String getNama_pertemuan() {
        return nama_pertemuan;
    }

    public void setNama_pertemuan(String nama_pertemuan) {
        this.nama_pertemuan = nama_pertemuan;
    }

    public String getId_m_connectgroup() {
        return id_m_connectgroup;
    }

    public void setId_m_connectgroup(String id_m_connectgroup) {
        this.id_m_connectgroup = id_m_connectgroup;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getId_pemimpin() {
        return id_pemimpin;
    }

    public void setId_pemimpin(String id_pemimpin) {
        this.id_pemimpin = id_pemimpin;
    }

    public String getNama_pemimpin() {
        return nama_pemimpin;
    }

    public void setNama_pemimpin(String nama_pemimpin) {
        this.nama_pemimpin = nama_pemimpin;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNama_connectgroup() {
        return nama_connectgroup;
    }

    public void setNama_connectgroup(String nama_connectgroup) {
        this.nama_connectgroup = nama_connectgroup;
    }
}
