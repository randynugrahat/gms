package com.skripsi.randynt.gms.ProfilePage;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.Admin.AdminEventActivity;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.AdminEventMenuListAdapter;
import com.skripsi.randynt.gms.classDef.AdminEventMenu;
import com.skripsi.randynt.gms.classDef.ProfileMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ChatcategoryActivity extends AppCompatActivity {
    private ArrayList<AdminEventMenu> menuList;
    private ListView mListView;
    private String userID;
    private int alreadyHaveRoom;
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatcategory);
        mListView = (ListView)findViewById(android.R.id.list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        SharedPreferences sharedPref = this.getSharedPreferences("userInfo",0);
        this.userID = sharedPref.getString("userid","0");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("Counselling Category");



    }
    @Override
    public void onResume(){
        super.onResume();

        loadParticipant();

    }
    public void  setAdapter(){
        AdminEventMenuListAdapter adapter = new AdminEventMenuListAdapter(this.getBaseContext(),R.layout.adapter_profile_layout,menuList);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Class cls =menuList.get(position).getAct();
                Intent intent = new Intent(ChatcategoryActivity.this, cls);
                startActivity(intent);

            }
        });
    }
    public void setMenu(final int room)
    {
        if(room != 1) {
            menuList.add(new AdminEventMenu("ic_person_pin_red_40dp", "Family", NewChatroomActivity.class));
            menuList.add(new AdminEventMenu("ic_school_red_40dp", "School", NewChatroomActivity.class));
            menuList.add(new AdminEventMenu("ic_sharing_black_40dp", "Sharing", NewChatroomActivity.class));
            menuList.add(new AdminEventMenu("ic_other_red_40dp", "Others", NewChatroomActivity.class));
            setAdapter();
        }
        else {
            finish();
        }
    }

    private void loadParticipant() {

        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"chatparticipant?id="+userID+"&type=1",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray commentdata = jsonObject.getJSONArray("data");
                            Log.e("Lnegth",Integer.toString(length));
                            if(length>0)
                            {
                                alreadyHaveRoom=1;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        setMenu(alreadyHaveRoom);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        menuList = new ArrayList<>();
                        setMenu(0);
                    }
                });

        Volley.newRequestQueue(this).add(stringRequest);

    }
}
