package com.skripsi.randynt.gms.ContentPageTab2;

import android.content.Context;
import android.media.Image;
import android.net.Uri;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.skripsi.randynt.gms.R;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailDevotionActivity extends AppCompatActivity {
    Bundle extra ;
    private ImageView devotionImage;
    private VideoView devotionVideo;
    private MaterialButton playBtn;
    private TextView devotionTitle;
    private TextView devotionContent;
    private String devotionType="";
    private String devotionTitleText;
    private String devotionKeterangan;
    private String devotionUrl;
    private Context mCtx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_devotion);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mCtx = this;
        devotionImage =(ImageView) findViewById(R.id.devotionImage);
        devotionVideo =(VideoView) findViewById(R.id.devotionVideo);
        devotionTitle =(TextView) findViewById(R.id.devotionTitle);
        devotionContent =(TextView) findViewById(R.id.devotionKeterangan);
        playBtn =(MaterialButton) findViewById(R.id.devotionPlayButton);

        extra = getIntent().getExtras();
        String newsExtras =extra.getString("devotionDetail");

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(newsExtras);
            JSONObject devData = jsonObject.getJSONObject("Devotion");
//            Log.e("Extrass", jsonObject.toString());
            devotionType = devData.get("type").toString();
            devotionTitleText = devData.get("nama").toString();
            devotionKeterangan = devData.get("keterangan").toString();
            devotionUrl = devData.get("path").toString();
            devotionTitle.setText(devotionTitleText);
            devotionContent.setText(devotionKeterangan);
            CheckType(devData.get("type").toString());
            if(devotionType.equals("Image")) {

                Glide.with(mCtx)
                        .load(devotionUrl)
                        .into(devotionImage);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }



        playBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Uri uri = Uri.parse(devotionUrl);
                Log.e("URL",uri.toString());
                devotionVideo.setVideoURI(uri);
                devotionVideo.start();
            }
        });
    }

    public void CheckType(final String type) {
        Log.e("DvotionType",devotionType);
        if(type.equals("Verse"))
        {
            Log.e("type","verse");
            playBtn.setVisibility(View.GONE);
            devotionImage.setVisibility(View.GONE);
            devotionVideo.setVisibility(View.GONE);
        }
        else if (type.equals("Video"))
        {   Log.e("type","video");
            playBtn.setVisibility(View.VISIBLE);
            devotionVideo.setVisibility(View.VISIBLE);
            devotionImage.setVisibility(View.GONE);
        }
        else if (type.equals("Image"))
        {
            Log.e("type","Image");
            playBtn.setVisibility(View.GONE);
            devotionVideo.setVisibility(View.GONE);
            devotionImage.setVisibility(View.VISIBLE);
        }
    }
}
