package com.skripsi.randynt.gms.ProfilePage;

import android.content.SharedPreferences;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;

import java.util.HashMap;
import java.util.Map;

public class ChangePassActivity extends AppCompatActivity {

    EditText oldpass;
    EditText newpass;
    EditText confpass;
    private String oldpassword;
    private String newpassword;
    private String confpassword;
    private String userID;
    private String URL_PRODUCTS;
    MaterialButton changeButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        this.URL_PRODUCTS = "https://nxtxdev.xyz/api/pass";

        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","");

        oldpass=  (EditText) findViewById(R.id.oldpass);
        newpass=  (EditText) findViewById(R.id.newpass);
        confpass=  (EditText) findViewById(R.id.newpassconf);
        changeButton =findViewById(R.id.changepass_btn);

        changeButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                oldpassword=oldpass.getText().toString();
                newpassword=newpass.getText().toString();
                confpassword=confpass.getText().toString();
                checkoldpass(oldpassword,newpassword,confpassword);
            }
        });
    }

    private void changepass(String pass) {
        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        final String passwordfinal = pass;
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(getApplicationContext(),"Change password success",Toast.LENGTH_LONG).show();
                        oldpass.setText("");
                        newpass.setText("");
                        confpass.setText("");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),"Connection Error",Toast.LENGTH_LONG).show();
                    }
                })
        {
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id",userID);
                params.put("password",passwordfinal);
                return params;
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void checkoldpass(String passold, final String newpassword, final String confpassword) {
        final String pass = passold;
        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(newpassword.equalsIgnoreCase(confpassword))
                        {
                            changepass(newpassword);
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"New Password and Confirmation not match",Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error",error.toString());
                        Log.e("pass",pass);
                        Log.e("userid",userID);
                        Toast.makeText(getApplicationContext(),"Wrong Old Password",Toast.LENGTH_LONG).show();
                    }
                })
        {
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id",userID);
                params.put("password",pass);
                return params;
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
}
