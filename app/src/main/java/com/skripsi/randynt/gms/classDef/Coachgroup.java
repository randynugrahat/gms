package com.skripsi.randynt.gms.classDef;

public class Coachgroup {
    private String id_m_coachgroup;
    private String nama_coachgroup;
    private String nama_coach;
    private String id_m_audience_fk;

    public Coachgroup(String id_m_coachgroup, String nama_coachgroup, String nama_coach, String id_m_audience_fk) {
        this.id_m_coachgroup = id_m_coachgroup;
        this.nama_coachgroup = nama_coachgroup;
        this.nama_coach = nama_coach;
        this.id_m_audience_fk = id_m_audience_fk;
    }

    public String getId_m_coachgroup() {
        return id_m_coachgroup;
    }

    public void setId_m_coachgroup(String id_m_coachgroup) {
        this.id_m_coachgroup = id_m_coachgroup;
    }

    public String getNama_coachgroup() {
        return nama_coachgroup;
    }

    public void setNama_coachgroup(String nama_coachgroup) {
        this.nama_coachgroup = nama_coachgroup;
    }

    public String getNama_coach() {
        return nama_coach;
    }

    public void setNama_coach(String nama_coach) {
        this.nama_coach = nama_coach;
    }

    public String getId_m_audience_fk() {
        return id_m_audience_fk;
    }

    public void setId_m_audience_fk(String id_m_audience_fk) {
        this.id_m_audience_fk = id_m_audience_fk;
    }

    @Override
    public String toString() {
        return "Coachgroup{" +
                "id_m_coachgroup='" + id_m_coachgroup + '\'' +
                ", nama_coachgroup='" + nama_coachgroup + '\'' +
                ", nama_coach='" + nama_coach + '\'' +
                ", id_m_audience_fk='" + id_m_audience_fk + '\'' +
                '}';
    }
}
