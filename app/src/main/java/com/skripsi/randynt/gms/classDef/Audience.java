package com.skripsi.randynt.gms.classDef;

public class Audience {
    private int id;
    private String nama;
    private String nama_panggilan;
    private String user_name;
    private int user_level;
    private String sex;
    private String tanggal_lahir;
    private String tempat_lahir;
    private String alamat;
    private int id_provinces;
    private int id_regencies;
    private int id_districts;
    private int id_villages;
    private String no_telp;

    public Audience(int id, String nama, String nama_panggilan, String user_name, int user_level, String sex, String tanggal_lahir, String tempat_lahir, String alamat, int id_provinces, int id_regencies, int id_districts, int id_villages, String no_telp) {
        this.id = id;
        this.nama = nama;
        this.nama_panggilan = nama_panggilan;
        this.user_name = user_name;
        this.user_level = user_level;
        this.sex = sex;
        this.tanggal_lahir = tanggal_lahir;
        this.tempat_lahir = tempat_lahir;
        this.alamat = alamat;
        this.id_provinces = id_provinces;
        this.id_regencies = id_regencies;
        this.id_districts = id_districts;
        this.id_villages = id_villages;
        this.no_telp = no_telp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama_panggilan() {
        return nama_panggilan;
    }

    public void setNama_panggilan(String nama_panggilan) {
        this.nama_panggilan = nama_panggilan;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getUser_level() {
        return user_level;
    }

    public void setUser_level(int user_level) {
        this.user_level = user_level;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public int getId_provinces() {
        return id_provinces;
    }

    public void setId_provinces(int id_provinces) {
        this.id_provinces = id_provinces;
    }

    public int getId_regencies() {
        return id_regencies;
    }

    public void setId_regencies(int id_regencies) {
        this.id_regencies = id_regencies;
    }

    public int getId_districts() {
        return id_districts;
    }

    public void setId_districts(int id_districts) {
        this.id_districts = id_districts;
    }

    public int getId_villages() {
        return id_villages;
    }

    public void setId_villages(int id_villages) {
        this.id_villages = id_villages;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    @Override
    public String toString() {
        return "Audience{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", nama_panggilan='" + nama_panggilan + '\'' +
                ", user_name='" + user_name + '\'' +
                ", user_level=" + user_level +
                ", sex='" + sex + '\'' +
                ", tanggal_lahir='" + tanggal_lahir + '\'' +
                ", tempat_lahir='" + tempat_lahir + '\'' +
                ", alamat='" + alamat + '\'' +
                ", id_provinces=" + id_provinces +
                ", id_regencies=" + id_regencies +
                ", id_districts=" + id_districts +
                ", id_villages=" + id_villages +
                ", no_telp='" + no_telp + '\'' +
                '}';
    }
}
