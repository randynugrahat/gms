package com.skripsi.randynt.gms.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.skripsi.randynt.gms.CgReport.TabCgAttendanceFragment;
import com.skripsi.randynt.gms.CgReport.TabMemberlistFragment;
import com.skripsi.randynt.gms.CgReport.TabReportFragment;

public class CgReportPagerAdapter extends FragmentStatePagerAdapter {

        int mNoOfTabs;

        public CgReportPagerAdapter(FragmentManager fm, int NumberOfTabs)
        {
            super(fm);
            this.mNoOfTabs = NumberOfTabs;
        }


        @Override
        public Fragment getItem(int position) {
            switch(position)
            {

                case 0:
                    TabReportFragment tab2 = new TabReportFragment();
                    return  tab2;
                case 1:
                    TabCgAttendanceFragment tab1 = new TabCgAttendanceFragment();
                    return  tab1;
                case 2:
                    TabMemberlistFragment tab3 = new TabMemberlistFragment();
                    return  tab3;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNoOfTabs;
        }
    }