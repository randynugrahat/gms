package com.skripsi.randynt.gms.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.skripsi.randynt.gms.UserReport.TabCgreportFragment;
import com.skripsi.randynt.gms.UserReport.TabEventreportFragment;
import com.skripsi.randynt.gms.UserReport.TabMinistryreportFragment;
import com.skripsi.randynt.gms.UserReport.TabProfilereportFragment;

public class ReportPagerAdapter extends FragmentStatePagerAdapter {

    int mNoOfTabs;

    public ReportPagerAdapter(FragmentManager fm, int NumberOfTabs)
    {
        super(fm);
        this.mNoOfTabs = NumberOfTabs;
    }


    @Override
    public Fragment getItem(int position) {
        switch(position)
        {

            case 0:
                TabProfilereportFragment tab1 = new TabProfilereportFragment();
                return tab1;
            case 1:
                TabCgreportFragment tab2 = new TabCgreportFragment();
                return  tab2;
            case 2:
                TabEventreportFragment tab3 = new TabEventreportFragment();
                return  tab3;
            case 3:
                TabMinistryreportFragment tab4 = new TabMinistryreportFragment();
                return  tab4;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }
}