package com.skripsi.randynt.gms.CgReport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.AdminCG.DetailCgAttendActivity;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.CgAttendListAdapter;
import com.skripsi.randynt.gms.classDef.CgMeeting;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TabCgAttendanceFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TabCgAttendanceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabCgAttendanceFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<CgMeeting> cgMeetList = new ArrayList<>();
    private String idcg;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/pertemuancg?";
    private ListView mListView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public TabCgAttendanceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TabCgAttendanceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TabCgAttendanceFragment newInstance(String param1, String param2) {
        TabCgAttendanceFragment fragment = new TabCgAttendanceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    private void setAdapter(ArrayList<CgMeeting> cgMeet){
        CgAttendListAdapter adapter = new CgAttendListAdapter(getActivity().getBaseContext(),R.layout.adapter_cgattend_layout,cgMeet);
        mListView.setAdapter(adapter);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_tab_cg_attendance, container, false);
                mListView = (ListView)view.findViewById(android.R.id.list);


        SharedPreferences sharedPref = this.getActivity().getSharedPreferences("userInfo",MODE_PRIVATE);
        this.idcg = sharedPref.getString("userid_m_audience","0");
        loadEvent();
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String value= cgMeetList.get(position).toString();
                Intent intent = new Intent(getActivity(), DetailCgAttendActivity.class);
                intent.putExtra("cgattenddetail", value);
                startActivity(intent);
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void loadEvent()
    {
        String URL_PRODUCTS1 = URL_PRODUCTS+"id="+idcg+"&type=2";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                cgMeetList.add(new CgMeeting(
                                        cgmeet.getString("id_pertemuan_cg"),
                                        cgmeet.getString("nama_pertemuan"),
                                        cgmeet.getString("id_m_connectgroup"),
                                        cgmeet.getString("tanggal"),
                                        cgmeet.getString("place"),
                                        cgmeet.getString("id_audience"),
                                        cgmeet.getString("nama_m_audience"),
                                        cgmeet.getString("keterangan"),
                                        cgmeet.getString("nama_connectgroup")

                                ));

                            }
                            setAdapter(cgMeetList);


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getBaseContext()).add(stringRequest);
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
