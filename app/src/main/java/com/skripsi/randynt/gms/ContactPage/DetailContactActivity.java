package com.skripsi.randynt.gms.ContactPage;

import android.content.Intent;
import android.net.Uri;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Contact;
import com.skripsi.randynt.gms.classDef.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailContactActivity extends AppCompatActivity {
    Bundle extra;
    private int userID;
    private ArrayList<User> userList = new ArrayList<>();
    TextView profileName;
    TextView connectgroup;
    TextView name_textview;
    TextView email_textview;
    TextView phone_textview;
    TextView birth_textview;
    TextView address_textview;
    MaterialButton callButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_contact);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        profileName=(TextView)findViewById(R.id.profileName);
        connectgroup=(TextView)findViewById(R.id.connectgroup);
        name_textview=(TextView)findViewById(R.id.name_textview);
        email_textview=(TextView)findViewById(R.id.email_textview);
        phone_textview=(TextView)findViewById(R.id.phone_textview);
        birth_textview=(TextView)findViewById(R.id.birth_textview);
        address_textview=(TextView)findViewById(R.id.address_textview);
        callButton = (MaterialButton)findViewById(R.id.callbtn);
        extra = getIntent().getExtras();
        userID =extra.getInt("contactdetail");
        loadUserdata();
        callButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String phone = phone_textview.getText().toString();
                Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts(
                        "tel", phone, null));
                startActivity(phoneIntent);
            }
        });
    }
    private void loadUserdata()
    {
        String URL_PRODUCTS = "https://nxtxdev.xyz/api/user?id="+this.userID+"&type=1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray userdataarray = jsonObject.getJSONArray("data");
                            JSONObject userdata = userdataarray.getJSONObject(0);

                            userList.add(new User(
                                    userdata.getInt("id_m_audience"),
                                    userdata.getString("nama_m_audience"),
                                    userdata.getString("tanggal_lahir"),
                                    userdata.getString("alamat"),
                                    userdata.getString("email"),
                                    userdata.getString("nama_connectgroup"),
                                    userdata.getString("no_telp")
                            ));
                            Log.e("userdataList",userList.get(0).toString());
                            profileName.setText(userList.get(0).getNama_audience());
                            name_textview.setText(userList.get(0).getNama_audience());
                            connectgroup.setText(userList.get(0).getConnectgroup());
                            email_textview.setText(userList.get(0).getEmail());
                            phone_textview.setText(userList.get(0).getPhone());
                            birth_textview.setText(userList.get(0).getTgl_lahir());
                            address_textview.setText(userList.get(0).getAlamat());


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
