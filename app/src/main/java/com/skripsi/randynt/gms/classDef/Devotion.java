package com.skripsi.randynt.gms.classDef;

public class Devotion {
    private int id_devotion;
    private String tanggal;
    private String nama;
    private String keterangan;
    private String path;
    private String type;

    public Devotion(int id_devotion, String tanggal, String nama, String keterangan, String path, String type) {
        this.id_devotion = id_devotion;
        this.tanggal = tanggal;
        this.nama = nama;
        this.keterangan = keterangan;
        this.path = path;
        this.type = type;
    }

    @Override
    public String toString() {
        return "{Devotion:{" +
                "'id_devotion':" + id_devotion +
                ", 'tanggal':'" + tanggal + '\'' +
                ", 'nama':'" + nama + '\'' +
                ", 'keterangan':'" + keterangan + '\'' +
                ", 'path':'" + path + '\'' +
                ", 'type':'" + type + '\'' +
                '}'+
                '}';
    }

    public int getId_devotion() {
        return id_devotion;
    }

    public void setId_devotion(int id_devotion) {
        this.id_devotion = id_devotion;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
