package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Contact;
import com.skripsi.randynt.gms.classDef.Event;

import java.util.List;

public class EventListAdapter extends ArrayAdapter<Event> {
    private static final String TAG = "EventListAdapter";
    private Context mContext;
    int mResource;

    public EventListAdapter(@NonNull Context context, int resource, @NonNull List<Event> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        int idEvent = getItem(position).getId_event();
        String nama = getItem(position).getNama();
        String tanggal = getItem(position).getTanggal();
        String keterangan = getItem(position).getKeterangan();
        String tempat = getItem(position).getTempat();
        String alamat = getItem(position).getAlamat();
        double longitude =getItem(position).getLongitude();
        double latitude =getItem(position).getLatitude();

        Event event = new Event(idEvent,nama,tanggal,keterangan,tempat,alamat,longitude,latitude);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        // TextView TvPicture =(TextView) convertView.findViewById(R.id.name_textview);
        TextView TvName =(TextView) convertView.findViewById(R.id.eventname_textview);
        TextView TvKeterangan =(TextView) convertView.findViewById(R.id.eventketerangan_textview);

        // TvPicture.setText(Picture);
        TvName.setText(nama);
        TvKeterangan.setText(keterangan);
        return convertView;
    }

}
