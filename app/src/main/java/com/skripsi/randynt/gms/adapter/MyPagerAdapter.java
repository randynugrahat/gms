package com.skripsi.randynt.gms.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.skripsi.randynt.gms.contentPage.Tab1;
import com.skripsi.randynt.gms.contentPage.Tab2;
import com.skripsi.randynt.gms.contentPage.Tab3;

public class MyPagerAdapter extends FragmentStatePagerAdapter {

    int mNoOfTabs;

    public MyPagerAdapter(FragmentManager fm, int NumberOfTabs)
    {
        super(fm);
        this.mNoOfTabs = NumberOfTabs;
    }


    @Override
    public Fragment getItem(int position) {
        switch(position)
        {

            case 0:
                Tab1 tab1 = new Tab1();
                return tab1;
            case 1:
                Tab2 tab2 = new Tab2();
                return  tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }
}