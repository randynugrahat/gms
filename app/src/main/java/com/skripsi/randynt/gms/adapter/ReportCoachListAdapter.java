package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Coachgroup;

import java.util.List;

    public class ReportCoachListAdapter extends ArrayAdapter<Coachgroup> {
        private static final String TAG = "ContactListAdapter";
        private Context mContext;
        int mResource;

        public ReportCoachListAdapter(@NonNull Context context, int resource, @NonNull List<Coachgroup> objects) {
            super(context, resource, objects);
            this.mContext = context;
            this.mResource = resource;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            //String Picture = getItem(position).getPicture();

            String nama_coachgroup = getItem(position).getNama_coachgroup();
            String nama_coach = getItem(position).getNama_coach();
            String id_m_coachgroup = getItem(position).getId_m_coachgroup();
            String id_m_audience_fk = getItem(position).getId_m_audience_fk();
            Coachgroup coachgroup = new Coachgroup(id_m_coachgroup,nama_coachgroup,nama_coach,id_m_audience_fk);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource,parent,false);

            TextView Tvpemimpin =(TextView) convertView.findViewById(R.id.eventketerangan_textview);
            TextView TvName =(TextView) convertView.findViewById(R.id.eventname_textview);

            Tvpemimpin.setText(nama_coach);
            TvName.setText(nama_coachgroup);
            return convertView;
        }
    }

