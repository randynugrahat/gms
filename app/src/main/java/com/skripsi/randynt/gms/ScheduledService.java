package com.skripsi.randynt.gms;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ScheduledService extends Service
{

    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/rsvp";
    private Timer timer = new Timer();
    private String userID;
    private final String CHANNEL_ID = "Event Reminder";
    private final int NOTIFICATION_ID =001;
    private String id_rsvp;

    public void displayNotification(final String Text)
    {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this,CHANNEL_ID);
        builder.setSmallIcon(R.drawable.logo);
        builder.setContentTitle("Event Reminder");
        builder.setContentText(Text);
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICATION_ID,builder.build());
    }
    @Override
    public IBinder onBind(Intent intent)
    {
        return null;
    }

    @Override
    public void onCreate()
    {
        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        userID = sharedPref.getString("userid","0");

        super.onCreate();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH");
                final String formattedDate = df.format(c.getTime());
            checkrsvp(formattedDate);
            }
        }, 0, 10*1000);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    private void checkrsvp(final String time) {
        String[] chatTime =time.split(" ");
        String timeLink=chatTime[0]+"%20"+chatTime[1];
        String URL_LINK = URL_PRODUCTS+"?id="+userID+"&reminder="+timeLink;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_LINK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray commentdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject comment = commentdata.getJSONObject(i);
                                Log.e("DATA", comment.getString("nama_event"));
                                id_rsvp = comment.getString("id_event_audience");
                                displayNotification(comment.getString("nama_event"));
                            }

                            remindUpdate();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
    private void remindUpdate() {
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                })
        {
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id",id_rsvp);
                params.put("reminder","1");
                return params;
            }
        };

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
}