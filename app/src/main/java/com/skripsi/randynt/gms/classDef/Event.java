package com.skripsi.randynt.gms.classDef;

public class Event {
    private int id_event;
    private String nama;
    private String tanggal;
    private String keterangan;
    private String tempat;
    private String alamat;
    private double longitude;
    private double latitude;

    public Event(int id_event, String nama, String tanggal, String keterangan, String tempat, String alamat, double longitude, double latitude) {
        this.id_event = id_event;
        this.nama = nama;
        this.tanggal = tanggal;
        this.keterangan = keterangan;
        this.tempat = tempat;
        this.alamat = alamat;
        this.longitude = longitude;
        this.latitude = latitude;
    }


    public int getId_event() {
        return id_event;
    }

    public void setId_event(int id_event) {
        this.id_event = id_event;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "{Event:{" +
                "'id_event':" + id_event +
                ", 'nama':'" + nama + '\'' +
                ", 'tanggal':'" + tanggal + '\'' +
                ", 'keterangan':'" + keterangan + '\'' +
                ", 'tempat':'" + tempat + '\'' +
                ", 'alamat':'" + alamat + '\'' +
                ", 'longitude':" + longitude +
                ", 'latitude':" + latitude +
                '}'+
                '}';
    }


}
