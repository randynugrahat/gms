package com.skripsi.randynt.gms.AdminEvent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.EventMinistryListAdapter;
import com.skripsi.randynt.gms.adapter.UserMinistryListAdapter;
import com.skripsi.randynt.gms.classDef.EventMinistry;
import com.skripsi.randynt.gms.classDef.UserMinistry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class UserEventMinistryActivity extends AppCompatActivity {
    private ArrayList<EventMinistry> userministryList;
    Bundle extra;
    String eventDetail;
    private String nama_pertemuan;
    private String keterangan;
    private String tanggal;
    private String id_event;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/";
    TextView nama_textview ;
    TextView tanggal_textview ;
    TextView keterangan_textview ;
    private ListView mListView;

    private List<String> ministryList = new ArrayList<String>();;
    private List<Integer> ministryListID = new ArrayList<Integer>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loadMinistry();
        setContentView(R.layout.activity_user_event_ministry);
        extra = getIntent().getExtras();
        eventDetail =extra.getString("eventDetail");
        try {
            JSONObject jsonObject = new JSONObject(eventDetail);
            JSONObject eventdata = jsonObject.getJSONObject("Event");
            nama_pertemuan = eventdata.get("nama").toString();
            tanggal = eventdata.get("tanggal").toString();
            keterangan = eventdata.get("keterangan").toString();
            id_event = eventdata.get("id_event").toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        nama_textview=(TextView)findViewById(R.id.textView_pertemuan);
        tanggal_textview=(TextView)findViewById(R.id.textView_tanggal);
        keterangan_textview=(TextView)findViewById(R.id.textView_keterangan);
        nama_textview.setText(nama_pertemuan);
        tanggal_textview.setText(tanggal);
        keterangan_textview.setText(keterangan);
        mListView = (ListView)findViewById(android.R.id.list);


        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_add:
                        Intent intent= new Intent(UserEventMinistryActivity.this,AddUserMinistryActivity.class);
                        intent.putExtra("idevent", id_event);
                        intent.putStringArrayListExtra("ministrydata", (ArrayList<String>) ministryList);
                        intent.putIntegerArrayListExtra("ministrydataID", (ArrayList<Integer>) ministryListID);
                        startActivity(intent);
                        return true;
                }
                return false;
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    @Override
    public void onResume(){
        super.onResume();
        loadUser();

    }
    private void setAdapter(ArrayList<EventMinistry> cgAttend){
        EventMinistryListAdapter adapter = new EventMinistryListAdapter(this,R.layout.adapter_eventdetail_attend,cgAttend);
        mListView.setAdapter(adapter);
    }
    private void loadUser()
    {
        userministryList = new ArrayList<>();
        URL_PRODUCTS=URL_PRODUCTS+"eventministry?id="+id_event+"&type=1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject meet = eventdata.getJSONObject(i);
                                userministryList.add(new EventMinistry(
                                        meet.getString("id_event_ministry"),
                                        meet.getString("id_audience_fk"),
                                        meet.getString("id_event_fk"),
                                        meet.getString("id_m_ministry"),
                                        meet.getString("nama"),
                                        meet.getString("nama_event"),
                                        meet.getString("tanggal_mulai"),
                                        meet.getString("nama_m_audience")
                                ));

                            }
                            setAdapter(userministryList);


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
    private void loadMinistry()
    {
        String URL_Place = this.URL_PRODUCTS+"ministry";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_Place,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray userdata = jsonObject.getJSONArray("data");
                            for (int i = 0; i < length; i++) {
                                JSONObject user = userdata.getJSONObject(i);
                                ministryList.add(
                                        user.getString("nama"));
                                ministryListID.add(user.getInt("id_m_ministry"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.e("MENU",menu.toString());
        getMenuInflater().inflate(R.menu.add_user_ministry, menu);
        return true;
    }
}
