package com.skripsi.randynt.gms.EventPage;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Contact;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class DetailEventActivity extends AppCompatActivity implements OnMapReadyCallback {
    Bundle extra ;
    String eventDetail;
    String eventPlace;
    String eventName;
    String eventTanggal;
    String eventKeterangan;
    TextView title ;
    TextView place;
    TextView alamat;
    TextView date;
    TextView desc;
    double longitude;
    double latitude;
    private String userID;
    private String eventID;
    private String URL_PRODUCTS = "";
    MaterialButton rsvpButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_event);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rsvpButton =findViewById(R.id.rsvp_btn);
        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","");
        extra = getIntent().getExtras();
        eventDetail =extra.getString("eventdetail");
        title=(TextView)findViewById(R.id.textView_title);
        date =(TextView)findViewById(R.id.textView_date);
        place=(TextView)findViewById(R.id.textView_place);
        alamat=(TextView)findViewById(R.id.textView_address);
        desc=(TextView)findViewById(R.id.textView_description);
        Log.e("eventDetail",eventDetail);
        try {
            JSONObject jsonObject = new JSONObject(eventDetail);
            JSONObject eventdata = jsonObject.getJSONObject("Event");
            Log.e("JSONobj",eventdata.toString());
            Log.e("eventdataa",eventdata.get("nama").toString() );
            title.setText(eventdata.get("nama").toString());
            date.setText(eventdata.get("tanggal").toString());
            place.setText(eventdata.get("tempat").toString());
            alamat.setText(eventdata.get("alamat").toString());
            desc.setText(eventdata.get("keterangan").toString());
            eventID = eventdata.get("id_event").toString();
            longitude =Double.parseDouble(eventdata.get("longitude").toString());
            latitude =Double.parseDouble(eventdata.get("latitude").toString());
            eventPlace=eventdata.get("tempat").toString();
            this.URL_PRODUCTS = "https://nxtxdev.xyz/api/rsvp?id="+this.eventID+"&iduser="+this.userID;
            checkrsvp();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        rsvpButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                 rsvp(userID,eventID);
                checkrsvp();
            }
        });
    }


    @Override
    public void onMapReady(GoogleMap map) {
        LatLng mark = new LatLng(latitude, longitude);

        map.addMarker(new MarkerOptions().position(mark).title(eventPlace));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(mark, 18    ));
    }
    private void rsvp(String userID, String eventID){
        final String iduser = userID;
        final String idevent = eventID;

            StringRequest request = new StringRequest(Request.Method.POST, "https://nxtxdev.xyz/api/rsvp",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {

                                JSONObject jsonObject = new JSONObject(response);

                                JSONObject userdata = jsonObject.getJSONObject("data");

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("id_event_fk",idevent);
                    params.put("id_m_audience_fk",iduser);
                    return params;
                }
            };
            Volley.newRequestQueue(this).add(request);



    }
    private void checkrsvp() {

        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        rsvpButton.setEnabled(false);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
}
