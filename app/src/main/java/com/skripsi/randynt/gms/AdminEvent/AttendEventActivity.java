package com.skripsi.randynt.gms.AdminEvent;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.EventAttendListAdapter;
import com.skripsi.randynt.gms.classDef.EventVolunteer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AttendEventActivity extends AppCompatActivity {
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/";
    private ListView mListView;
    private String userID;
    private ArrayList<EventVolunteer> eventvolunteerList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attend_event);

        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","");

        loadEvent();
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("");
        mListView = (ListView)findViewById(android.R.id.list);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                String value= eventvolunteerList.get(position).toString();
                Intent intent = new Intent(AttendEventActivity.this, DetailEventAttendActivity.class);
                intent.putExtra("eventattenddetail", value);
                startActivity(intent);
            }
        });
    }
    private void setAdapter(ArrayList<EventVolunteer> cgMeet){
        EventAttendListAdapter adapter = new EventAttendListAdapter(this,R.layout.adapter_event_attend_list_layout,eventvolunteerList);
        mListView.setAdapter(adapter);
    }
    private void loadEvent()
    {
        String URL_PRODUCTS1 = URL_PRODUCTS+"eventministry?id="+userID+"&type=2";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                eventvolunteerList.add(new EventVolunteer(
                                        cgmeet.getString("id_event_ministry"),
                                        cgmeet.getString("id_m_ministry_fk"),
                                        cgmeet.getString("id_event_fk"),
                                        cgmeet.getString("nama_event"),
                                        cgmeet.getString("tanggal_mulai"),
                                        cgmeet.getString("keterangan_event"),
                                        cgmeet.getString("id_pengajaran_type_fk")

                                ));

                            }
                            setAdapter(eventvolunteerList);


                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
