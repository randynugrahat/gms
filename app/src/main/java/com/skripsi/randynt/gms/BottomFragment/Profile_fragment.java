package com.skripsi.randynt.gms.BottomFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.Admin.AdminActivity;
import com.skripsi.randynt.gms.ProfilePage.ChangePassActivity;
import com.skripsi.randynt.gms.ProfilePage.ChatcategoryActivity;
import com.skripsi.randynt.gms.ProfilePage.MyProfileActivity;
import com.skripsi.randynt.gms.ProfilePage.MyqrcodeActivity;
import com.skripsi.randynt.gms.ProfilePage.NewChatroomActivity;
import com.skripsi.randynt.gms.ProfilePage.UserCouncellingActivity;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.ProfileMenuListAdapter;
import com.skripsi.randynt.gms.classDef.ProfileMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Profile_fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Profile_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Profile_fragment extends ListFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<ProfileMenu> profileList;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private int userlevel;
    private int userihub;
    private int userlistener;
    private String userID;
    private int alreadyHaveRoom;
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/";
    private OnFragmentInteractionListener mListener;
    ListView mListView;
    public Profile_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Profile_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Profile_fragment newInstance(String param1, String param2) {
        Profile_fragment fragment = new Profile_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
//        Object obj = global_list.getAdapter().getItem(position);
        String value= profileList.get(position).toString();
        Class cls =profileList.get(position).getAct();
        Log.e("vala",value);
        Intent intent= new Intent(getActivity(),cls);

        startActivity(intent);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_profile_fragment, container, false);
        mListView= (ListView) view.findViewById(android.R.id.list);


        SharedPreferences sharedPref = getActivity().getSharedPreferences("userInfo",0);
        this.userID = sharedPref.getString("userid","");
        this.userlevel = Integer.parseInt(sharedPref.getString("user_level","0"));
        this.userlistener =Integer.parseInt(sharedPref.getString("listener","0"));
        this.userihub = Integer.parseInt(sharedPref.getString("user_ihub","0"));
//        loadParticipant(userlevel,userihub,userlistener);


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    public void setAdapter(){
        ProfileMenuListAdapter adapter = new ProfileMenuListAdapter(getActivity().getBaseContext(),R.layout.adapter_profile_layout,profileList);
        mListView.setAdapter(adapter);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onResume() {
        super.onResume();
        profileList = new ArrayList<>();
//        Log.e("Onresume","Resume");
        loadParticipant(userlevel,userihub,userlistener);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public void setMenu(final int level,final int ihub,final int listener) {
        profileList.add(new ProfileMenu("ic_person_red_40dp", "My Profile", MyProfileActivity.class));
        profileList.add(new ProfileMenu("ic_pass_red_40dp", "Change Password", ChangePassActivity.class));
        profileList.add(new ProfileMenu("ic_qrcode_red_40dp", "My QR Code", MyqrcodeActivity.class));
        if (level > 0 || ihub == 1 || listener == 1) {
            profileList.add(new ProfileMenu("ic_administrator_red_40dp", "Administrative Area", AdminActivity.class));
        }

        if (alreadyHaveRoom == 1) {
            profileList.add(new ProfileMenu("ic_chat_red_40dp", "Counselling", UserCouncellingActivity.class));
            }
            else{
            profileList.add(new ProfileMenu("ic_chat_red_40dp", "Counselling", ChatcategoryActivity.class));

        }

            setAdapter();
    }

    private void loadParticipant(final int level,final int ihub,final int listener) {

        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"chatparticipant?id="+userID+"&type=1",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray commentdata = jsonObject.getJSONArray("data");
                            Log.e("Lnegth",Integer.toString(length));
                            if(length>0)
                            {
                                alreadyHaveRoom=1;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        setMenu(level,ihub,listener);
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        setMenu(level,ihub,listener);
                    }
                });

        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);

    }
}
