package com.skripsi.randynt.gms.AttendPage;

import android.content.SharedPreferences;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.CgEval;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DetailUserCgAttendActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Spinner spinner_pemimpin;
    Spinner spinner_pertemuan;
    EditText keterangan;
    String keteranganText;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/cgevaluation";
    private String iduser;
    private String nilai_pertemuan;
    private String nilai_pemimpin;
    private String id_pertemuan_cg;
    private String nama_pertemuan;
    private TextView pertemuancg;
    private ArrayList<CgEval> cgEvals = new ArrayList<>();

    private TextView nilai_pemimpinText;
    private TextView nilai_pertemuanText;
    private TextView keteranganTextview;
    private TextView namaTextview;
    LinearLayout edit_layout;
    LinearLayout view_layout;
    MaterialButton saveButton;
    Bundle extra ;
    String eventDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user_cg_attend);

        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.iduser = sharedPref.getString("userid","");

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("");

        extra = getIntent().getExtras();
        eventDetail =extra.getString("eventdetail");

        saveButton =findViewById(R.id.saveevaluasi_btn);
        spinner_pemimpin = (Spinner) findViewById(R.id.spinner_nilai_pemimpin);
        spinner_pertemuan = (Spinner) findViewById(R.id.spinner_pertemuan);
        keterangan = (EditText) findViewById(R.id.keterangan);
        pertemuancg = (TextView)findViewById(R.id.cgevent_title);
        edit_layout = (LinearLayout)findViewById(R.id.layout_addattend);
        view_layout = (LinearLayout)findViewById(R.id.layout_attend);


        nilai_pertemuanText = (TextView) findViewById(R.id.nilaipertemuan_eval_textview);
        nilai_pemimpinText = (TextView) findViewById(R.id.nilaipemimpin_eval_textview);
        keteranganTextview = (TextView) findViewById(R.id.keterangan_eval_textview);
        namaTextview = (TextView)findViewById(R.id.nama_eval_textview) ;

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.spinner_number, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_pemimpin.setAdapter(adapter);
        spinner_pertemuan.setAdapter(adapter);
        spinner_pemimpin.setOnItemSelectedListener(this);
        spinner_pertemuan.setOnItemSelectedListener(this);
        try {
            JSONObject jsonObject = new JSONObject(eventDetail);
            JSONObject eventdata = jsonObject.getJSONObject("AttendanceCg");
            nama_pertemuan = eventdata.get("nama_pertemuan").toString();
            id_pertemuan_cg = eventdata.get("id_pertemuan_cg").toString();
            pertemuancg.setText(nama_pertemuan);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        evaluation();
        saveButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                keteranganText=keterangan.getText().toString();
                addEval(keteranganText);
            }
        });

    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
//        this.id_pemimpin = userListID.get(pos).toString();
        Spinner spin = (Spinner) parent;
        Spinner spin2 = (Spinner) parent;

        if (spin.getId() == R.id.spinner_nilai_pemimpin) {
            nilai_pemimpin = parent.getItemAtPosition(pos).toString();
        }
        if (spin2.getId() == R.id.spinner_pertemuan) {
            nilai_pertemuan = parent.getItemAtPosition(pos).toString();
        }


    }



    public void onNothingSelected(AdapterView<?> parent) {

//        Toast.makeText(AddCgActivity.this,"Nothing",Toast.LENGTH_LONG);
    }

    private void addEval(final String keteranganT){

        StringRequest request = new StringRequest(Request.Method.POST, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject userdata = jsonObject.getJSONObject("data");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(),"Add Evaluation Success",Toast.LENGTH_LONG).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("id_m_audience_fk",iduser);
                        Log.e("id_pertemuan_cg_fk",id_pertemuan_cg);
                        Log.e("nilai_pertemuan",nilai_pertemuan);
                        Log.e("nilai_pemimpin",nilai_pemimpin);
                        Log.e("keterangan_evaluation",keteranganT);
                        Log.e("Error",error.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id_m_audience_fk",iduser);
                params.put("id_pertemuan_cg_fk",id_pertemuan_cg);
                params.put("nilai_pertemuan",nilai_pertemuan);
                params.put("nilai_pemimpin",nilai_pemimpin);
                params.put("keterangan_evaluation",keteranganT);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);



    }
    public void setLayoutInvisible() {
        if (edit_layout.getVisibility() == View.VISIBLE) {
            edit_layout.setVisibility(View.GONE);
            view_layout.setVisibility(View.VISIBLE);
        }
    }
    public void setLayoutVisible() {
        if (edit_layout.getVisibility() == View.GONE) {
            edit_layout.setVisibility(View.VISIBLE);
            view_layout.setVisibility(View.GONE);

        }
    }
    private void evaluation() {

        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        String URL_PRODUCTS1 = URL_PRODUCTS+"?id="+iduser+"&idpertemuan="+id_pertemuan_cg+"&type=1";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");

                            for (int i = 0; i < length; i++) {

                                JSONObject cgmeet = eventdata.getJSONObject(i);
                                cgEvals.add(new CgEval(
                                        cgmeet.getString("id_cg_eval"),
                                        cgmeet.getString("id_m_audience_fk"),
                                        cgmeet.getString("id_pertemuan_cg_fk"),
                                        cgmeet.getString("nilai_pertemuan"),
                                        cgmeet.getString("nilai_pemimpin"),
                                        cgmeet.getString("keterangan_evaluation"),
                                        cgmeet.getString("nama_m_audience"),
                                        cgmeet.getString("nama_pertemuan"),
                                        cgmeet.getString("tanggal")

                                ));
                                Log.e("cgmeet",cgEvals.get(0).toString());
                            }
                            namaTextview.setText(cgEvals.get(0).getNama_pertemuan());
                            nilai_pertemuanText.setText(cgEvals.get(0).getNilai_pertemuan());
                            nilai_pemimpinText.setText(cgEvals.get(0).getNilai_pemimpin());
                            keteranganTextview.setText(cgEvals.get(0).getKeterangan());
                            setLayoutInvisible();
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
}
