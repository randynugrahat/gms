package com.skripsi.randynt.gms.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.skripsi.randynt.gms.AttendPage.Attend1Fragment;
import com.skripsi.randynt.gms.AttendPage.Attend2Fragment;

public class AttendPagerAdapter extends FragmentStatePagerAdapter {

    int mNoOfTabs;

    public AttendPagerAdapter(FragmentManager fm, int NumberOfTabs)
    {
        super(fm);
        this.mNoOfTabs = NumberOfTabs;
    }


    @Override
    public Fragment getItem(int position) {
        switch(position)
        {

            case 0:
                Attend1Fragment tab1 = new Attend1Fragment();
                return tab1;
            case 1:
                Attend2Fragment tab2 = new Attend2Fragment();
                return  tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNoOfTabs;
    }
}