package com.skripsi.randynt.gms.BottomFragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.EventPage.DetailEventActivity;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.EventListAdapter;
import com.skripsi.randynt.gms.classDef.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Event_fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Event_fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Event_fragment extends ListFragment {
    private static final String URL_PRODUCTS = "https://nxtxdev.xyz/api/events";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ArrayList<Event> eventList;
    private  ListView global_list;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private  ListView mListView;
    private OnFragmentInteractionListener mListener;

    public Event_fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Event_fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Event_fragment newInstance(String param1, String param2) {
        Event_fragment fragment = new Event_fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
//        Object obj = global_list.getAdapter().getItem(position);
        String value= eventList.get(position).toString();
        Log.e("vala",value);
        Intent intent= new Intent(getActivity(),DetailEventActivity.class);
        intent.putExtra("eventdetail", value);
        startActivity(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_event_fragment, container, false);
       mListView = (ListView) view.findViewById(android.R.id.list);


//        loadEvent();
        final SwipeRefreshLayout pullToRefresh = view.findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadEvent();
                pullToRefresh.setRefreshing(false);
            }
        });
       // this.global_list = mListView;
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    private void setAdapter(ArrayList<Event> event){
        EventListAdapter adapter = new EventListAdapter(getActivity().getBaseContext(),R.layout.adapter_event_layout,event);
        mListView.setAdapter(adapter);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadEvent();

    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void loadEvent() {

        eventList = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray eventdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject event = eventdata.getJSONObject(i);
                                eventList.add(new Event(
                                        event.getInt("id_event"),
                                        event.getString("nama_event"),
                                        event.getString("tanggal_mulai"),
                                        event.getString("keterangan_event"),
                                        event.getString("nama_m_place"),
                                        event.getString("alamat"),
                                        event.getDouble("longitude"),
                                        event.getDouble("latitude")
                                ));

                                Log.e("eventnameE: ", eventList.get(i).toString());
                            }

                            setAdapter(eventList);
                         /*   if(adapter == null){
                                adapter = new NewsRecyclerViewAdapter(newsList);
                                recyclerView.setAdapter(adapter);
                            }else{
                                adapter.notifyDataSetChanged();
                            }*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(getActivity().getApplicationContext()).add(stringRequest);
        int listSize = eventList.size();
        Log.isLoggable("Size",eventList.size());
        for (int i = 0; i<listSize; i++){

        }
    }
}
