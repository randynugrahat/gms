package com.skripsi.randynt.gms.classDef;

public class Connectgroup {
    private String id_connectgroup;
    private String nama_connectgroup;
    private String id_coachgroup_fk;
    private String nama_pemimpin;

    public Connectgroup(String id_connectgroup, String nama_connectgroup, String id_coachgroup_fk, String nama_pemimpin) {
        this.id_connectgroup = id_connectgroup;
        this.nama_connectgroup = nama_connectgroup;
        this.id_coachgroup_fk = id_coachgroup_fk;
        this.nama_pemimpin = nama_pemimpin;
    }

    public String getId_connectgroup() {
        return id_connectgroup;
    }

    public void setId_connectgroup(String id_connectgroup) {
        this.id_connectgroup = id_connectgroup;
    }

    public String getNama_connectgroup() {
        return nama_connectgroup;
    }

    public void setNama_connectgroup(String nama_connectgroup) {
        this.nama_connectgroup = nama_connectgroup;
    }

    public String getId_coachgroup_fk() {
        return id_coachgroup_fk;
    }

    public void setId_coachgroup_fk(String id_coachgroup_fk) {
        this.id_coachgroup_fk = id_coachgroup_fk;
    }

    public String getNama_pemimpin() {
        return nama_pemimpin;
    }

    public void setNama_pemimpin(String nama_pemimpin) {
        this.nama_pemimpin = nama_pemimpin;
    }

    @Override
    public String toString() {
        return "Connectgroup{" +
                "id_connectgroup='" + id_connectgroup + '\'' +
                ", nama_connectgroup='" + nama_connectgroup + '\'' +
                ", id_coachgroup_fk='" + id_coachgroup_fk + '\'' +
                ", nama_pemimpin='" + nama_pemimpin + '\'' +
                '}';
    }
}
