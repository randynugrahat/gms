package com.skripsi.randynt.gms.classDef;

public class Comment {
    private int id_comment;
    private int id_news;
    private int id_audience;
    private String audience_name;
    private String name;
    private String date;

    public Comment(int id_comment, int id_news, int id_audience, String audience_name, String name, String date) {
        this.id_comment = id_comment;
        this.id_news = id_news;
        this.id_audience = id_audience;
        this.audience_name = audience_name;
        this.name = name;
        this.date = date;
    }

    public int getId_comment() {
        return id_comment;
    }

    public void setId_comment(int id_comment) {
        this.id_comment = id_comment;
    }

    public int getId_news() {
        return id_news;
    }

    public void setId_news(int id_news) {
        this.id_news = id_news;
    }

    public int getId_audience() {
        return id_audience;
    }

    public void setId_audience(int id_audience) {
        this.id_audience = id_audience;
    }

    public String getAudience_name() {
        return audience_name;
    }

    public void setAudience_name(String audience_name) {
        this.audience_name = audience_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id_comment=" + id_comment +
                ", id_news=" + id_news +
                ", id_audience=" + id_audience +
                ", audience_name='" + audience_name + '\'' +
                ", name='" + name + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
