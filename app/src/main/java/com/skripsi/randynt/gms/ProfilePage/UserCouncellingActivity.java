package com.skripsi.randynt.gms.ProfilePage;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.MessageListAdapter;
import com.skripsi.randynt.gms.classDef.Message;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserCouncellingActivity extends AppCompatActivity {
    private RecyclerView mMessageRecycler;
    private MessageListAdapter mMessageAdapter;
    private List<Message> messageList;
    private String id_chatroom;
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/";
    private String userID;
    private String last_time;
    private EditText chatInput;
    Button send;
    Context mctx;
    Handler h = new Handler();
    int delay = 2*1000;
    Runnable runnable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_councelling);
        chatInput=(EditText)findViewById(R.id.edittext_chatbox);
        send=(Button)findViewById(R.id.button_chatbox_send);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        messageList = new ArrayList<>();
        mctx=this;
        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","");
        loadParticipant();
        send.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {

               postChat(chatInput.getText().toString());

            }
        });
    }
    @Override
    protected void onResume() {

        h.postDelayed( runnable = new Runnable() {
            public void run() {
                if(messageList.size() == 0)
                {
                    loadMessage(id_chatroom,"0");
                }
                else {
                    loadMessage(id_chatroom, last_time);
                }
                h.postDelayed(runnable, delay);
            }
        }, delay);

        super.onResume();
    }

    @Override
    protected void onPause() {
        h.removeCallbacks(runnable);
        super.onPause();
    }
    private void setAdapter(List<Message> message){
        mMessageRecycler = (RecyclerView) findViewById(R.id.reyclerview_message_list);
//        mMessageRecycler.setHasFixedSize(true);
        mMessageRecycler.setLayoutManager(new LinearLayoutManager(this));
        mMessageAdapter = new MessageListAdapter(this, message,"user");
        mMessageRecycler.setAdapter(mMessageAdapter);
        mMessageRecycler.scrollToPosition(mMessageAdapter.getItemCount()-1);

    }
    private void loadParticipant() {

        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS+"chatparticipant?id="+userID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray commentdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject comment = commentdata.getJSONObject(i);
                               id_chatroom =comment.getString("id_chatroom_fk");
                            }

                            loadMessage(id_chatroom,"0");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void loadMessage(final String chatroom,final String time) {

        String lastTimeChat = time;
        String URL_LINK;
        if(lastTimeChat.equals("0"))
        {
            URL_LINK = URL_PRODUCTS+"message?id="+chatroom+"&type=1";
        }
        else{
            String[] chatTime =time.split(" ");
            String timeLink=chatTime[0]+"%20"+chatTime[1];
            URL_LINK = URL_PRODUCTS+"message?id="+chatroom+"&type=1&time="+timeLink;
            Log.e("URL-Link",URL_LINK);
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_LINK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray commentdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject comment = commentdata.getJSONObject(i);
                                messageList.add(new Message(
                                        comment.getString("id_message"),
                                        comment.getString("id_chatroom_fk"),
                                        comment.getString("id_m_audience_fk"),
                                        comment.getString("nama_m_audience"),
                                        comment.getString("message"),
                                        comment.getString("status_message"),
                                        comment.getString("created_message"),
                                        comment.getString("message_type")
                                ));
                                if(i == length-1) {
                                    last_time = comment.getString("created_message");
                                }
                            }

//                            mMessageAdapter.notifyDataSetChanged();
                        setAdapter(messageList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }
    private void postChat(final String textinput){

        StringRequest request = new StringRequest(Request.Method.POST, URL_PRODUCTS+"message",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            JSONObject userdata = jsonObject.getJSONObject("data");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        chatInput.setText("");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id_m_audience_fk",userID);
                params.put("id_chatroom_fk",id_chatroom);
                params.put("message",textinput);
                params.put("message_type","user");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);



    }
}
