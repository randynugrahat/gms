package com.skripsi.randynt.gms.ContentPageTab1;

import android.content.SharedPreferences;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.CommentListAdapter;
import com.skripsi.randynt.gms.classDef.Comment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommentActivity extends AppCompatActivity {
    Bundle extra ;
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/newscomment";

    private String getlink;
    private ArrayList<Comment> commentList;
    private String news_id;
    private String userID;
    private String commentinputText;
    MaterialButton postCommentButton;
    EditText commentinput;
    ListView mListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        extra = getIntent().getExtras();
        news_id = extra.getString("newsid");
        getlink = URL_PRODUCTS.concat("?id="+news_id);

        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","");

        postCommentButton=(MaterialButton)findViewById(R.id.post_comment_btn);
        commentinput = (EditText)findViewById(R.id.comment_edittext);

         mListView= (ListView) findViewById(R.id.comment_list);

        loadComment();

        postCommentButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                commentinputText = commentinput.getText().toString();
                postComment(commentinputText);

            }
        });
    }

    private void setAdapter()
    {

        CommentListAdapter adapter = new CommentListAdapter(CommentActivity.this,R.layout.adapter_comment_layout,commentList);
//        adapter.notifyDataSetChanged();
        mListView.setAdapter(adapter);
    }


    private void loadComment() {

        /*  * Creating a String Request
         * The request type is GET defined by first parameter
         * The URL is defined in the second parameter
         * Then we have a Response Listener and a Error Listener
         * In response listener we will get the JSON response as a String
         * */
        commentList =new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, getlink,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray commentdata = jsonObject.getJSONArray("data");


                            for (int i = 0; i < length; i++) {

                                JSONObject comment = commentdata.getJSONObject(i);
                                commentList.add(new Comment(
                                        comment.getInt("id"),
                                        comment.getInt("id_content_news"),
                                        comment.getInt("id_m_audience"),
                                        comment.getString("nama_m_audience"),
                                        comment.getString("keterangan_comment"),
                                        comment.getString("created_comment")
                                ));

                                Log.e("CommentList: ", commentList.get(i).toString());
                            }

                            setAdapter();
                         /*   if(adapter == null){
                                adapter = new NewsRecyclerViewAdapter(newsList);
                                recyclerView.setAdapter(adapter);
                            }else{
                                adapter.notifyDataSetChanged();
                            }*/

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);

    }

    private void postComment(final String textinput){

        StringRequest request = new StringRequest(Request.Method.POST, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            JSONObject userdata = jsonObject.getJSONObject("data");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        commentinput.setText("");
                        loadComment();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id_m_audience",userID);
                params.put("id_content_news",news_id);
                params.put("keterangan_comment",textinput);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);



    }
}
