package com.skripsi.randynt.gms.classDef;

public class UserMinistry {
    private String id_user_ministry;
    private String id_m_audience;
    private String id_m_ministry;
    private String nama_ministry;
    private String nama_audience;

    public UserMinistry(String id_user_ministry, String id_m_audience, String id_m_ministry, String nama_ministry, String nama_audience) {
        this.id_user_ministry = id_user_ministry;
        this.id_m_audience = id_m_audience;
        this.id_m_ministry = id_m_ministry;
        this.nama_ministry = nama_ministry;
        this.nama_audience = nama_audience;
    }

    @Override
    public String toString() {
        return "UserMinistry{" +
                "id_user_ministry='" + id_user_ministry + '\'' +
                ", id_m_audience='" + id_m_audience + '\'' +
                ", id_m_ministry='" + id_m_ministry + '\'' +
                ", nama_ministry='" + nama_ministry + '\'' +
                ", nama_audience='" + nama_audience + '\'' +
                '}';
    }

    public String getId_user_ministry() {
        return id_user_ministry;
    }

    public void setId_user_ministry(String id_user_ministry) {
        this.id_user_ministry = id_user_ministry;
    }

    public String getId_m_audience() {
        return id_m_audience;
    }

    public void setId_m_audience(String id_m_audience) {
        this.id_m_audience = id_m_audience;
    }

    public String getId_m_ministry() {
        return id_m_ministry;
    }

    public void setId_m_ministry(String id_m_ministry) {
        this.id_m_ministry = id_m_ministry;
    }

    public String getNama_ministry() {
        return nama_ministry;
    }

    public void setNama_ministry(String nama_ministry) {
        this.nama_ministry = nama_ministry;
    }

    public String getNama_audience() {
        return nama_audience;
    }

    public void setNama_audience(String nama_audience) {
        this.nama_audience = nama_audience;
    }
}
