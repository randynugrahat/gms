package com.skripsi.randynt.gms.classDef;

public class AdminCgMenu {
    private String icon;
    private String menudesc;
    private Class act;

    public AdminCgMenu(String icon, String menudesc, Class act) {
        this.icon = icon;
        this.menudesc = menudesc;
        this.act = act;
    }

    @Override
    public String toString() {
        return "AdminCgMenu{" +
                "icon='" + icon + '\'' +
                ", menudesc='" + menudesc + '\'' +
                ", act=" + act +
                '}';
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getMenudesc() {
        return menudesc;
    }

    public void setMenudesc(String menudesc) {
        this.menudesc = menudesc;
    }

    public Class getAct() {
        return act;
    }

    public void setAct(Class act) {
        this.act = act;
    }
}
