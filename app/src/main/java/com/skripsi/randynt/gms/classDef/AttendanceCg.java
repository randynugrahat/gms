package com.skripsi.randynt.gms.classDef;

public class AttendanceCg {
    private String id_cg_attendance;
    private String id_pertemuan_cg;
    private String id_m_audience;
    private String time_in;
    private String keterangan;
    private String nama_pertemuan;
    private String tanggal_pertemuan;
    private String place;
    private String nama_audience;

    public AttendanceCg(String id_cg_attendance, String id_pertemuan_cg, String id_m_audience, String time_in, String keterangan, String nama_pertemuan, String tanggal_pertemuan, String place, String nama_audience) {
        this.id_cg_attendance = id_cg_attendance;
        this.id_pertemuan_cg = id_pertemuan_cg;
        this.id_m_audience = id_m_audience;
        this.time_in = time_in;
        this.keterangan = keterangan;
        this.nama_pertemuan = nama_pertemuan;
        this.tanggal_pertemuan = tanggal_pertemuan;
        this.place = place;
        this.nama_audience = nama_audience;
    }

    @Override
    public String toString() {
        return "{AttendanceCg:{" +
                "'id_cg_attendance':'" + id_cg_attendance + '\'' +
                ", 'id_pertemuan_cg':'" + id_pertemuan_cg + '\'' +
                ", 'id_m_audience':'" + id_m_audience + '\'' +
                ", 'time_in':'" + time_in + '\'' +
                ", 'keterangan':'" + keterangan + '\'' +
                ", 'nama_pertemuan':'" + nama_pertemuan + '\'' +
                ", 'tanggal_pertemuan':'" + tanggal_pertemuan + '\'' +
                ", 'place':'" + place + '\'' +
                ", 'nama_audience':'" + nama_audience + '\'' +
                '}'+
                '}';
    }

    public String getId_cg_attendance() {
        return id_cg_attendance;
    }

    public void setId_cg_attendance(String id_cg_attendance) {
        this.id_cg_attendance = id_cg_attendance;
    }

    public String getId_pertemuan_cg() {
        return id_pertemuan_cg;
    }

    public void setId_pertemuan_cg(String id_pertemuan_cg) {
        this.id_pertemuan_cg = id_pertemuan_cg;
    }

    public String getId_m_audience() {
        return id_m_audience;
    }

    public void setId_m_audience(String id_m_audience) {
        this.id_m_audience = id_m_audience;
    }

    public String getTime_in() {
        return time_in;
    }

    public void setTime_in(String time_in) {
        this.time_in = time_in;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getNama_pertemuan() {
        return nama_pertemuan;
    }

    public void setNama_pertemuan(String nama_pertemuan) {
        this.nama_pertemuan = nama_pertemuan;
    }

    public String getTanggal_pertemuan() {
        return tanggal_pertemuan;
    }

    public void setTanggal_pertemuan(String tanggal_pertemuan) {
        this.tanggal_pertemuan = tanggal_pertemuan;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getNama_audience() {
        return nama_audience;
    }

    public void setNama_audience(String nama_audience) {
        this.nama_audience = nama_audience;
    }
}
