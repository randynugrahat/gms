package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.CgMeeting;

import java.util.List;
    public class CgAttendListAdapter extends ArrayAdapter<CgMeeting> {
        private static final String TAG = "EventListAdapter";
        private Context mContext;
        int mResource;

        public CgAttendListAdapter(@NonNull Context context, int resource, @NonNull List<CgMeeting> objects) {
            super(context, resource, objects);
            this.mContext = context;
            this.mResource = resource;
        }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                //String Picture = getItem(position).getPicture();

                String idCGMeeting = getItem(position).getId_pertemuan_cg();
                String nama_pertemuan = getItem(position).getNama_pertemuan();
                String id_m_cg = getItem(position).getId_m_connectgroup();
                String tanggal = getItem(position).getTanggal();
                String place = getItem(position).getPlace();
                String id_pemimpin = getItem(position).getId_pemimpin();
                String nama_pemimpin = getItem(position).getNama_pemimpin();
                String keterangan = getItem(position).getKeterangan();
                String nama_cg = getItem(position).getNama_connectgroup();

                Log.e("CGadapter",getItem(position).getNama_pertemuan());
                CgMeeting cgMeeting = new CgMeeting(idCGMeeting,nama_pertemuan,id_m_cg,tanggal,place,id_pemimpin,nama_pemimpin,keterangan,nama_cg);
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(mResource, parent, false);

                TextView TvName = (TextView) convertView.findViewById(R.id.cgmeetname_textview);
                TextView TvTanggal = (TextView) convertView.findViewById(R.id.cgmeettanggal_textview);
                TextView TvTempat = (TextView) convertView.findViewById(R.id.cgmeettempat_textview);

                // TvPicture.setText(Picture);
                TvName.setText(nama_pertemuan);
                TvTanggal.setText(tanggal);
                TvTempat.setText(place);
                return convertView;
            }
    }
