package com.skripsi.randynt.gms.Admin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.skripsi.randynt.gms.AdminFlc.ChatListActivity;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.AdminMenuListAdapter;
import com.skripsi.randynt.gms.classDef.AdminMenu;

import java.util.ArrayList;

public class AdminActivity extends AppCompatActivity {
    private ArrayList<AdminMenu> adminList;
    private ListView mListView;
    private int userlevel;
    private int userihub;
    private int userlistener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        mListView = (ListView)findViewById(android.R.id.list);
        adminList = new ArrayList<>();
        SharedPreferences sharedPref = getSharedPreferences("userInfo",0);
        this.userlevel = Integer.parseInt(sharedPref.getString("user_level","0"));
        this.userlistener =Integer.parseInt(sharedPref.getString("listener","0"));
        this.userihub = Integer.parseInt(sharedPref.getString("user_ihub","0"));
        setMenu(userlevel,userihub,userlistener);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("");

       AdminMenuListAdapter adapter = new AdminMenuListAdapter(this.getBaseContext(),R.layout.adapter_profile_layout,adminList);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Class cls =adminList.get(position).getAct();
                Intent intent = new Intent(AdminActivity.this, cls);
                startActivity(intent);
            }
        });

    }

    public void setMenu(final int level,final int ihub,final int listener) {
        if (level >= 1) {
            adminList.add(new AdminMenu("ic_cg_red_40dp", "Connect Group Management", AdminCgActivity.class));
        }
        if (ihub == 1) {
            adminList.add(new AdminMenu("ic_event_red_40dp", "Event Management", AdminEventActivity.class));
        }
        if (listener == 1) {
            adminList.add(new AdminMenu("ic_chat_red_40dp", "Family & Life Counsellling", ChatListActivity.class));
        }
    }
}
