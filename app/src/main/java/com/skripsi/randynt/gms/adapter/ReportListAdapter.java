package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Audience;

import java.util.List;


public class ReportListAdapter extends ArrayAdapter<Audience> {
    private static final String TAG = "ContactListAdapter";
    private Context mContext;
    int mResource;

    public ReportListAdapter(@NonNull Context context, int resource, @NonNull List<Audience> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        int idAudience = getItem(position).getId();
        int user_level = getItem(position).getUser_level();
        int id_district = getItem(position).getId_districts();
        int id_province = getItem(position).getId_provinces();
        int id_regency = getItem(position).getId_regencies();
        int id_villages = getItem(position).getId_villages();
        String nama = getItem(position).getNama();
        String nama_panggilan = getItem(position).getNama_panggilan();
        String user_name = getItem(position).getUser_name();
        String sex = getItem(position).getSex();
        String tanggal_lahir = getItem(position).getTanggal_lahir();
        String tempat_lahir = getItem(position).getTempat_lahir();
        String alamat = getItem(position).getAlamat();
        String no_telp = getItem(position).getNo_telp();
        Audience audience = new Audience(idAudience,nama,nama_panggilan,user_name,user_level,sex,tanggal_lahir,tempat_lahir,alamat,id_province,id_regency,id_district,id_villages,no_telp);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        // TextView TvPicture =(TextView) convertView.findViewById(R.id.name_textview);
        TextView TvName =(TextView) convertView.findViewById(R.id.fullname_textview);

        // TvPicture.setText(Picture);
        TvName.setText(nama);
        return convertView;
    }
}
