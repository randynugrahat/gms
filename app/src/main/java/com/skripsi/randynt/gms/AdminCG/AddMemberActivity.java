package com.skripsi.randynt.gms.AdminCG;

import android.content.SharedPreferences;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class AddMemberActivity extends AppCompatActivity {
    private String nama_audience_string;
    private String nama_panggilan_string;
    private String username_string;
    private String sex_string;
    private String tgl_lahir_string;
    private String tempat_lahir_string;
    private String alamat_string;
    private String telp_string;
    private String email_string;
    private EditText nama_audience;
    private EditText nama_panggilan;
    private EditText username;
    private EditText sex;
    private EditText tgl_lahir;
    private EditText tempat_lahir;
    private EditText alamat;
    private EditText telp;
    private EditText email;

    private String iduser;
    private String id_m_cg;
    private String id_m_cabang;

    MaterialButton saveButton;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);


        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.iduser = sharedPref.getString("userid","");

        this.id_m_cg = sharedPref.getString("userid_m_audience","");
        this.id_m_cabang = sharedPref.getString("userid_m_cabang","");

        nama_audience=  (EditText) findViewById(R.id.Nama);
        nama_panggilan=  (EditText) findViewById(R.id.nama_panggilan);
        username=  (EditText) findViewById(R.id.username);
        sex=  (EditText) findViewById(R.id.jeniskelamin);
        tgl_lahir=  (EditText) findViewById(R.id.tgl_lahir);
        tempat_lahir=  (EditText) findViewById(R.id.tempat_lahir);
        alamat=  (EditText) findViewById(R.id.alamat);
        telp=  (EditText) findViewById(R.id.no_telp);
        email=  (EditText) findViewById(R.id.email);
        saveButton =findViewById(R.id.Addmember_btn);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                nama_audience_string=nama_audience.getText().toString();
                nama_panggilan_string=nama_panggilan.getText().toString();
                username_string=username.getText().toString();
                sex_string=sex.getText().toString();
                tgl_lahir_string=tgl_lahir.getText().toString();
                tempat_lahir_string=tempat_lahir.getText().toString();
                alamat_string=alamat.getText().toString();
                telp_string=telp.getText().toString();
                email_string=email.getText().toString();
                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String formattedDate = df.format(c.getTime());
                addNewMember(formattedDate);
            }
        });
    }

    private void addNewMember(final String createdTime){

        StringRequest request = new StringRequest(Request.Method.POST, URL_PRODUCTS+"user",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject userdata = jsonObject.getJSONObject("data");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(),"Add New Member Success",Toast.LENGTH_LONG).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("nama_m_audience",nama_audience_string);
                params.put("nama_panggilan",nama_panggilan_string);
                params.put("user_name",username_string);
                params.put("sex",sex_string);
                params.put("tanggal_lahir",tgl_lahir_string);
                params.put("tempat_lahir",tempat_lahir_string);
                params.put("alamat",alamat_string);
                params.put("no_telp",telp_string);
                params.put("email",email_string);
                params.put("id_m_cabang",id_m_cabang);
                params.put("id_m_connectgroup",id_m_cg);
                params.put("created",createdTime);
                params.put("created_id",iduser);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);



    }
}
