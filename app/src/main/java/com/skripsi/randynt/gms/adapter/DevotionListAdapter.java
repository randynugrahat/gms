package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.Devotion;

import java.util.List;

public class DevotionListAdapter extends ArrayAdapter<Devotion> {
        private static final String TAG = "EventListAdapter";
        private Context mContext;
        int mResource;

        public DevotionListAdapter(@NonNull Context context, int resource, @NonNull List<Devotion> objects) {
            super(context, resource, objects);
            this.mContext = context;
            this.mResource = resource;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            //String Picture = getItem(position).getPicture();

            int id_devotion = getItem(position).getId_devotion();
            String nama = getItem(position).getNama();
            String tanggal = getItem(position).getTanggal();
            String keterangan = getItem(position).getKeterangan();
            String path = getItem(position).getPath();
            String type = getItem(position).getType();

            Devotion devotion = new Devotion(id_devotion,tanggal,nama,keterangan,path,type);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource,parent,false);

            // TextView TvPicture =(TextView) convertView.findViewById(R.id.name_textview);
            TextView TvName =(TextView) convertView.findViewById(R.id.eventname_textview);
            TextView TvKeterangan =(TextView) convertView.findViewById(R.id.eventketerangan_textview);

            // TvPicture.setText(Picture);
            TvName.setText(nama);
            TvKeterangan.setText(tanggal);
            return convertView;
        }

    }
