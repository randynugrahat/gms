package com.skripsi.randynt.gms.Admin;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.AdminCG.AddCgActivity;
import com.skripsi.randynt.gms.AdminCG.AddMemberActivity;
import com.skripsi.randynt.gms.AdminCG.AttendCgActivity;
import com.skripsi.randynt.gms.AdminCG.ReportActivity;
import com.skripsi.randynt.gms.AdminCG.ReportGembalaActivity;
import com.skripsi.randynt.gms.AdminCG.ReportpenilikActivity;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.adapter.AdminCgMenuListAdapter;
import com.skripsi.randynt.gms.classDef.AdminCgMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AdminCgActivity extends AppCompatActivity {
    private ArrayList<AdminCgMenu> menuList;
    private ListView mListView;
    private String URL_PRODUCTS="https://nxtxdev.xyz/api/";
    private List<String> userList;
    private List<Integer> userListID;
    private int userlevel;
    private String id_m_cg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_cg);

        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.id_m_cg = sharedPref.getString("userid_m_audience","");
        this.userlevel = Integer.parseInt(sharedPref.getString("user_level",""));
        mListView = (ListView)findViewById(android.R.id.list);
        menuList = new ArrayList<>();
        setMenu();
        userList = new ArrayList<String>();
        userListID = new ArrayList<Integer>();
        loadUserdata();
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("Administrative Area");


        AdminCgMenuListAdapter adapter = new AdminCgMenuListAdapter(this.getBaseContext(),R.layout.adapter_profile_layout,menuList);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Class cls =menuList.get(position).getAct();
                Intent intent = new Intent(AdminCgActivity.this, cls);
                intent.putStringArrayListExtra("pemimpindata",(ArrayList<String>) userList);
                intent.putIntegerArrayListExtra("pemimpindataID",(ArrayList<Integer>) userListID);
                startActivity(intent);
            }
        });
    }

    public void setMenu()
    {
        if(userlevel == 1) {
        menuList.add(new AdminCgMenu("ic_add_cg_meeting_red_40dp","Add Meeting",AddCgActivity.class));
        menuList.add(new AdminCgMenu("ic_cgattend_red_40dp","Attendance",AttendCgActivity.class));
        menuList.add(new AdminCgMenu("ic_add_member_red_40dp","Add Member",AddMemberActivity.class));

            menuList.add(new AdminCgMenu("ic_cgreport_red_40dp", "CG Report", ReportActivity.class));
        }
        else if(userlevel == 2){
            menuList.add(new AdminCgMenu("ic_cgreport_red_40dp", "CG Report", ReportpenilikActivity.class));
        }
        else if(userlevel == 3){
            menuList.add(new AdminCgMenu("ic_cgreport_red_40dp", "CG Report", ReportGembalaActivity.class));
        }
    }

    private void loadUserdata()
    {
        String URL_PRODUCTS = this.URL_PRODUCTS+"user?id="+this.id_m_cg+"&type=2";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int length = jsonObject.getInt("total");

                            JSONArray userdata = jsonObject.getJSONArray("data");
                            for (int i = 0; i < length; i++) {
                                JSONObject user = userdata.getJSONObject(i);
                                userList.add(
                                        user.getString("nama_m_audience"));
                                userListID.add(user.getInt("id_m_audience"));
                                Log.e("Userdatapemimpin", userList.get(i));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        //adding our stringrequest to queue
        Volley.newRequestQueue(this).add(stringRequest);
    }
}
