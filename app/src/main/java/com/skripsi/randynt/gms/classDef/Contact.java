package com.skripsi.randynt.gms.classDef;

public class Contact {
    private int id_contact;
    private int id_m_audience1;
    private int id_m_audience2;
    private String nama;
    private String nama_panggilan;
    private String no_telp;
    private String no_telp2;
    private String email;
    private String alamat;

    public Contact(int id_contact, int id_m_audience1, int id_m_audience2, String nama, String nama_panggilan, String no_telp, String no_telp2, String email, String alamat) {
        this.id_contact = id_contact;
        this.id_m_audience1 = id_m_audience1;
        this.id_m_audience2 = id_m_audience2;
        this.nama = nama;
        this.nama_panggilan = nama_panggilan;
        this.no_telp = no_telp;
        this.no_telp2 = no_telp2;
        this.email = email;
        this.alamat = alamat;
    }

    public int getId_contact() {
        return id_contact;
    }

    public void setId_contact(int id_contact) {
        this.id_contact = id_contact;
    }

    public int getId_m_audience1() {
        return id_m_audience1;
    }

    public void setId_m_audience1(int id_m_audience1) {
        this.id_m_audience1 = id_m_audience1;
    }

    public int getId_m_audience2() {
        return id_m_audience2;
    }

    public void setId_m_audience2(int id_m_audience2) {
        this.id_m_audience2 = id_m_audience2;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama_panggilan() {
        return nama_panggilan;
    }

    public void setNama_panggilan(String nama_panggilan) {
        this.nama_panggilan = nama_panggilan;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getNo_telp2() {
        return no_telp2;
    }

    public void setNo_telp2(String no_telp2) {
        this.no_telp2 = no_telp2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id_contact=" + id_contact +
                ", id_m_audience1=" + id_m_audience1 +
                ", id_m_audience2=" + id_m_audience2 +
                ", nama='" + nama + '\'' +
                ", nama_panggilan='" + nama_panggilan + '\'' +
                ", no_telp='" + no_telp + '\'' +
                ", no_telp2='" + no_telp2 + '\'' +
                ", email='" + email + '\'' +
                ", alamat='" + alamat + '\'' +
                '}';
    }
}
