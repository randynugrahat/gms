package com.skripsi.randynt.gms.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.skripsi.randynt.gms.GembalaReport.TabCoachListFragment;
import com.skripsi.randynt.gms.GembalaReport.TabGembalaReportFragment;

public class GembalaReportPagerAdapter extends FragmentStatePagerAdapter {

        int mNoOfTabs;

        public GembalaReportPagerAdapter(FragmentManager fm, int NumberOfTabs)
        {
            super(fm);
            this.mNoOfTabs = NumberOfTabs;
        }


        @Override
        public Fragment getItem(int position) {
            switch(position)
            {

                case 0:
                    TabGembalaReportFragment tab1 = new TabGembalaReportFragment();
                    return  tab1;
                case 1:
                    TabCoachListFragment tab2 = new TabCoachListFragment();
                    return  tab2;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNoOfTabs;
        }
    }