package com.skripsi.randynt.gms.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.ProfileMenu;

import java.util.List;

public class ProfileMenuListAdapter  extends ArrayAdapter<ProfileMenu> {
    private static final String TAG = "MenuListAdapter";
    private Context mContext;
    int mResource;

    public ProfileMenuListAdapter(@NonNull Context context, int resource, @NonNull List<ProfileMenu> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //String Picture = getItem(position).getPicture();

        String icon = getItem(position).getIcon();
        String menu = getItem(position).getMenudesc();
        Class act = getItem(position).getAct();
        ProfileMenu pm = new ProfileMenu(icon,menu,act);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        // TextView TvPicture =(TextView) convertView.findViewById(R.id.name_textview);
        TextView Menu =(TextView) convertView.findViewById(R.id.menu_textview);
        ImageView imgIcon = (ImageView) convertView.findViewById((R.id.iconView));
        int id = mContext.getResources().getIdentifier(icon, "drawable", mContext.getPackageName());
        // TvPicture.setText(Picture);
        imgIcon.setImageResource(id);
        Menu.setText(menu);
        return convertView;
    }
}
