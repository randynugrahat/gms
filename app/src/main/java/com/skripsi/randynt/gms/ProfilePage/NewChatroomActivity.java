package com.skripsi.randynt.gms.ProfilePage;

import android.content.SharedPreferences;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.skripsi.randynt.gms.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class NewChatroomActivity extends AppCompatActivity {
    MaterialButton addBtn;
    private String URL_PRODUCTS = "https://nxtxdev.xyz/api/";
    private String id_chatroom;
    private String userID;
    String formattedDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_chatroom);

        SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
        this.userID = sharedPref.getString("userid","");
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setTitle("New Chatroom");
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         formattedDate = df.format(c.getTime());
        addBtn=(MaterialButton)findViewById(R.id.createchatroom_btn);
        addBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                postChatroom(formattedDate);
            }
        });
    }

    private void postChatroom(final String textinput){

        StringRequest request = new StringRequest(Request.Method.POST, URL_PRODUCTS+"chatroom",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);

                            JSONArray commentdata = jsonObject.getJSONArray("data");
                            JSONObject comment = commentdata.getJSONObject(0);
                            id_chatroom =comment.getString("id_chatroom");
                            postChatparticipant(userID,id_chatroom);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("status_chatroom","1");
                params.put("created",textinput);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }

    private void postChatparticipant(final String user,final String room){

        StringRequest request = new StringRequest(Request.Method.POST, URL_PRODUCTS+"chatparticipant",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(),"Create New Room Success",Toast.LENGTH_LONG).show();
                        finish();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("id_m_audience_fk",user);
                params.put("id_chatroom_fk",room);
                params.put("participant_type","user");
                return params;
            }
        };
        Volley.newRequestQueue(this).add(request);
    }
}
