package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.analytics.ecommerce.Product;
import com.skripsi.randynt.gms.CustomItemClickListener;
import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.News;

import java.util.ArrayList;

public class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsRecyclerViewAdapter.ProductViewHolder> {
    private Context mCtx;
    private ArrayList<News> NewsList;
    CustomItemClickListener listener;
    public NewsRecyclerViewAdapter( Context ctx,ArrayList<News> NewsList, CustomItemClickListener listener) {
        this.mCtx=ctx;
        this.NewsList = NewsList;
        this.listener = listener;
    }

    @Override

    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.card_layout, parent, false);
        final ProductViewHolder mViewHolder = new ProductViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, mViewHolder.getPosition());
            }
        });
        return mViewHolder;
    }
    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        News news = NewsList.get(position);

        //loading the image
        Glide.with(mCtx)
                .load(news.getImage())
                .into(holder.imageView);

        holder.textViewTitle.setText(news.getNama());
        holder.textViewDate.setText(news.getTanggal());
    }

    @Override
    public int getItemCount() {
        return NewsList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewDate;
        ImageView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.news_title);
            textViewDate = itemView.findViewById(R.id.news_date);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }
}

