package com.skripsi.randynt.gms.classDef;

public class EventMinistry {
    private String id_event_ministry;
    private String id_audience_fk;
    private String id_event_fk;
    private String id_m_ministry_fk;
    private String nama_ministry;
    private String nama_event;
    private String tanggal_event;
    private String nama_audience;

    public EventMinistry(String id_event_ministry, String id_audience_fk, String id_event_fk, String id_m_ministry_fk, String nama_ministry, String nama_event, String tanggal_event, String nama_audience) {
        this.id_event_ministry = id_event_ministry;
        this.id_audience_fk = id_audience_fk;
        this.id_event_fk = id_event_fk;
        this.id_m_ministry_fk = id_m_ministry_fk;
        this.nama_ministry = nama_ministry;
        this.nama_event = nama_event;
        this.tanggal_event = tanggal_event;
        this.nama_audience = nama_audience;
    }

    public EventMinistry(String id_event_ministry, String id_audience_fk, String id_event_fk, String id_m_ministry_fk, String nama_ministry, String nama_event, String tanggal_event) {
        this.id_event_ministry = id_event_ministry;
        this.id_audience_fk = id_audience_fk;
        this.id_event_fk = id_event_fk;
        this.id_m_ministry_fk = id_m_ministry_fk;
        this.nama_ministry = nama_ministry;
        this.nama_event = nama_event;
        this.tanggal_event = tanggal_event;
    }

    public String getNama_audience() {
        return nama_audience;
    }

    public void setNama_audience(String nama_audience) {
        this.nama_audience = nama_audience;
    }

    public String getId_event_ministry() {
        return id_event_ministry;
    }

    public void setId_event_ministry(String id_event_ministry) {
        this.id_event_ministry = id_event_ministry;
    }

    public String getId_audience_fk() {
        return id_audience_fk;
    }

    public void setId_audience_fk(String id_audience_fk) {
        this.id_audience_fk = id_audience_fk;
    }

    public String getId_event_fk() {
        return id_event_fk;
    }

    public void setId_event_fk(String id_event_fk) {
        this.id_event_fk = id_event_fk;
    }

    public String getId_m_ministry_fk() {
        return id_m_ministry_fk;
    }

    public void setId_m_ministry_fk(String id_m_ministry_fk) {
        this.id_m_ministry_fk = id_m_ministry_fk;
    }

    public String getNama_ministry() {
        return nama_ministry;
    }

    public void setNama_ministry(String nama_ministry) {
        this.nama_ministry = nama_ministry;
    }

    public String getNama_event() {
        return nama_event;
    }

    public void setNama_event(String nama_event) {
        this.nama_event = nama_event;
    }

    public String getTanggal_event() {
        return tanggal_event;
    }

    public void setTanggal_event(String tanggal_event) {
        this.tanggal_event = tanggal_event;
    }

    @Override
    public String toString() {
        return "EventMinistry{" +
                "id_event_ministry='" + id_event_ministry + '\'' +
                ", id_audience_fk='" + id_audience_fk + '\'' +
                ", id_event_fk='" + id_event_fk + '\'' +
                ", id_m_ministry_fk='" + id_m_ministry_fk + '\'' +
                ", nama_ministry='" + nama_ministry + '\'' +
                ", nama_event='" + nama_event + '\'' +
                ", tanggal_event='" + tanggal_event + '\'' +
                '}';
    }
}
