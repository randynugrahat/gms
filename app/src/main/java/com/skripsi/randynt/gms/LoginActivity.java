package com.skripsi.randynt.gms;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    /*private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPrefEditor;*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final TextInputEditText username = findViewById(R.id.username_edit_text);
        final TextInputEditText password =findViewById(R.id.password_edit_text);
        MaterialButton nextButton =findViewById(R.id.next_button);


        nextButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                //Intent intent = new Intent(this, ContactActivity.class);
                // startActivity(intent);
                login(username.getText().toString(),password.getText().toString());

//                startActivity(new Inten  username.setText("");
//                password.setText("");t(getApplication(),ContactActivity.class));
//                }
//                else{
//                    Toast.makeText(getApplicationContext(),"Wrong Username/Password",Toast.LENGTH_LONG).show();
//                    password.setText("");
//                }
            }
        });

    }

    private void login(String username, final String password){
        final String uname = username;
        final String pass = password;
        final TextInputEditText usernameInput = findViewById(R.id.username_edit_text);
        final TextInputEditText passwordInput =findViewById(R.id.password_edit_text);
       /* sharedPreferences = getPreferences(MODE_PRIVATE);
        sharedPrefEditor = sharedPreferences.edit();*/
       final SharedPreferences sharedPref = getSharedPreferences("userInfo",MODE_PRIVATE);
       final SharedPreferences.Editor editor = sharedPref.edit();

       Toast.makeText(this,"SAVED!",Toast.LENGTH_LONG);
       final  Gson gson = new Gson();


        if (uname.isEmpty() || pass.isEmpty())
        {
            Toast.makeText(getApplicationContext(),"Username or Password Empty",Toast.LENGTH_LONG).show();
        }else
        {

            StringRequest request = new StringRequest(Request.Method.POST, "https://nxtxdev.xyz/api/authentication",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {

                                JSONObject jsonObject = new JSONObject(response);

                                JSONObject userdata = jsonObject.getJSONObject("data");
                                String userid = userdata.getString("id_m_audience");
                                String userid_m_audience = userdata.getString("id_m_connectgroup");
                                String userid_m_cabang = userdata.getString("id_m_cabang");
                                String userid_level = userdata.getString("user_level");
                                String userid_ihub = userdata.getString("ihub");
                                String userid_coach = userdata.getString("id_m_coachgrup");
                                String userid_cabang = userdata.getString("id_m_cabang");
                                String listener = userdata.getString("listener");
                                String nama_m_audience = userdata.getString("nama_m_audience");
                                String tgl_lahir = userdata.getString("tanggal_lahir");

                                editor.putString("userid",userid);
                                editor.putString("userid_m_audience",userid_m_audience);
                                editor.putString("userid_m_cabang",userid_m_cabang);
                                editor.putString("user_level",userid_level);
                                editor.putString("user_ihub",userid_ihub);
                                editor.putString("user_coach",userid_coach);
                                editor.putString("user_cabang",userid_cabang);
                                editor.putString("listener",listener);
                                editor.putString("nama_audience",nama_m_audience);
                                editor.putString("tgl_lahir",tgl_lahir);
                                editor.apply();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                             usernameInput.setText("");
                             passwordInput.setText("");
                            Intent intent = new Intent(LoginActivity.this, ContentActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                             passwordInput.setText("");
                            Log.e("Failed Login",error.toString());
                            Toast.makeText(getApplicationContext(),"Wrong Username/Password",Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("username",uname);
                    params.put("password",pass);
                    return params;
                }
            };
            Volley.newRequestQueue(this).add(request);

        }

    }


}
