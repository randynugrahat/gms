package com.skripsi.randynt.gms.classDef;

public class UserEventAttend {
    private String id_event_attendance;
    private String time_in;
    private String nama_m_audience;
    private String nama_event;
    private String tanggal_mulai;
    private String keterangan_event;

    public UserEventAttend(String id_event_attendance, String time_in, String nama_m_audience, String nama_event, String tanggal_mulai, String keterangan_event) {
        this.id_event_attendance = id_event_attendance;
        this.time_in = time_in;
        this.nama_m_audience = nama_m_audience;
        this.nama_event = nama_event;
        this.tanggal_mulai = tanggal_mulai;
        this.keterangan_event = keterangan_event;
    }

    @Override
    public String toString() {
        return "UserEventAttend{" +
                "id_event_attendance='" + id_event_attendance + '\'' +
                ", time_in='" + time_in + '\'' +
                ", nama_m_audience='" + nama_m_audience + '\'' +
                ", nama_event='" + nama_event + '\'' +
                ", tanggal_mulai='" + tanggal_mulai + '\'' +
                ", keterangan_event='" + keterangan_event + '\'' +
                '}';
    }

    public String getId_event_attendance() {
        return id_event_attendance;
    }

    public void setId_event_attendance(String id_event_attendance) {
        this.id_event_attendance = id_event_attendance;
    }

    public String getTime_in() {
        return time_in;
    }

    public void setTime_in(String time_in) {
        this.time_in = time_in;
    }

    public String getNama_m_audience() {
        return nama_m_audience;
    }

    public void setNama_m_audience(String nama_m_audience) {
        this.nama_m_audience = nama_m_audience;
    }

    public String getNama_event() {
        return nama_event;
    }

    public void setNama_event(String nama_event) {
        this.nama_event = nama_event;
    }

    public String getTanggal_mulai() {
        return tanggal_mulai;
    }

    public void setTanggal_mulai(String tanggal_mulai) {
        this.tanggal_mulai = tanggal_mulai;
    }

    public String getKeterangan_event() {
        return keterangan_event;
    }

    public void setKeterangan_event(String keterangan_event) {
        this.keterangan_event = keterangan_event;
    }
}
