package com.skripsi.randynt.gms.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.skripsi.randynt.gms.R;
import com.skripsi.randynt.gms.classDef.UserMinistry;

import java.util.List;

    public class UserMinistryListAdapter extends ArrayAdapter<UserMinistry> {
        private static final String TAG = "EventAttendListAdapter";
        private Context mContext;
        int mResource;
        public UserMinistryListAdapter(@NonNull Context context, int resource, @NonNull List<UserMinistry> objects) {
            super(context, resource, objects);
            this.mContext = context;
            this.mResource = resource;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            //String Picture = getItem(position).getPicture();

            String id_user_ministry = getItem(position).getId_user_ministry();
            String id_m_ministry = getItem(position).getId_m_ministry();
            String id_m_audience = getItem(position).getId_m_audience();
            String nama_audience = getItem(position).getNama_audience();
            String nama_ministry = getItem(position).getNama_ministry();

            UserMinistry eventMinistry = new UserMinistry(id_user_ministry,id_m_audience,id_m_ministry,nama_ministry,nama_audience);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(mResource, parent, false);

            TextView TvName = (TextView) convertView.findViewById(R.id.nama_textview);
            TextView TvTempat = (TextView) convertView.findViewById(R.id.timein_textview);

            // TvPicture.setText(Picture);
            TvName.setText(nama_audience);
            TvTempat.setText(nama_ministry);
            return convertView;
        }
    }